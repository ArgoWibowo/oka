<style type="text/css">
	h2{
		float:left;
	}
	hr{
		clear:both;
	}
	.tomboltombol{
		float:right;
		position: relative;
		top:24px;
	}

	#tomboledit{
		margin-right: 5px;
		width: 80px;
	}

	.table-borderless td{ 
	    border: 0 !important;
	}

	.table tbody tr{
	    background-color: white;
	}
	.labelin{
    font-weight: bold;
    vertical-align: middle;
  }

</style>
<h2>Detail Data Peserta OKA 2017</h2>
<div class="tomboltombol">
	<button class="btn btn-default" id="tomboledit" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Pesertas','action'=>'edit', $data['Peserta']['id']))?>'">Edit</button>
	<button class="btn btn-primary" id="tombolkembali" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Pesertas','action'=>'index'))?>'">Kembali</button>
</div>
<hr>
<table class="table table-borderless">
	<tbody>
		<tr><td class="labelin col-md-3">Nama</td><td style="width:2%;">:</td><td><?php echo $data['Peserta']['nama'];?></td></tr>
		<tr><td class="labelin">Prodi</td><td>:</td><td><?php echo $data['Prodi']['nama_prodi'];?></td></tr>
		<tr><td class="labelin">Kelompok</td><td>:</td><td><?php echo $data['Kelompok']['nama_kelompok'];?></td></tr>
		<tr><td class="labelin">Jenis kelamin</td><td>:</td><td><?php echo $data['Peserta']['jenis_kelamin']; ?></td></tr>
		<tr><td class="labelin">Tempat, tanggal lahir</td><td>:</td><td><?php echo $data['Peserta']['tempat_lahir'].", ".$data['Peserta']['tanggal_lahir']; ?></td></tr>
		<tr><td class="labelin">Golongan darah</td><td>:</td><td><?php echo $data['Peserta']['golongan_darah']; ?></td></tr>
		<tr><td class="labelin">Agama</td><td>:</td><td><?php echo $data['Peserta']['agama']; ?></td></tr>
		<tr><td class="labelin">Nomor telp</td><td>:</td><td><?php echo $data['Peserta']['nomor_telpon']; ?></td></tr>
		<tr><td class="labelin">Nomor orangtua</td><td>:</td><td><?php echo $data['Peserta']['nomor_orangtua']; ?></td></tr>
		<tr><td class="labelin">Alamat asal</td><td>:</td><td><?php echo $data['Peserta']['alamat_asal']; ?></td></tr>
		<tr><td class="labelin">Alamat di Yogya</td><td>:</td><td><?php echo $data['Peserta']['alamat_jogja']; ?></td></tr>
		<tr><td class="labelin">Asal sekolah</td><td>:</td><td><?php echo $data['Peserta']['asal_sekolah']; ?></td></tr>
		<tr><td class="labelin">Kota asal sekolah</td><td>:</td><td><?php echo $data['Peserta']['kota_asal_sekolah']; ?></td></tr>
		<tr><td class="labelin">Ukuran jas almamater</td><td>:</td><td><?php echo $data['Peserta']['ukuran_almamater']; ?></td></tr>
		<tr><td class="labelin">Alergi</td><td>:</td><td><?php echo $data['Peserta']['alergi']; ?></td></tr>
		<tr><td class="labelin">Penyakit yang sedang diderita</td><td>:</td><td><?php echo $data['Peserta']['penyakit_diderita']; ?></td></tr>
		<tr><td class="labelin">Alergi makanan</td><td>:</td><td><?php echo $data['Peserta']['alergi_makanan']; ?></td></tr>
		<tr><td class="labelin">Reaksi Alergi</td><td>:</td><td><?php echo $data['Peserta']['ralergi']; ?></td></tr>
		<tr><td class="labelin">Reaksi penyakit</td><td>:</td><td><?php echo $data['Peserta']['rpenyakit']; ?></td></tr>
		<tr><td class="labelin">Obat untuk pereda sakit atau alergi</td><td>:</td><td><?php echo $data['Peserta']['obat']; ?></td></tr>
		<tr><td class="labelin">Pengalaman baris-berbaris</td><td>:</td><td><?php echo $data['Peserta']['tonti']; ?></td></tr>
		<tr>
			<td class="labelin">Tanggal pendaftaran</td><td>:</td>
			<td><?php echo $data['Peserta']['created']; ?></td>
		</tr>
		<tr>
			<td class="labelin">Tanggal edit terakhir</td><td>:</td>
			<td><?php echo $data['Peserta']['modified']; ?></td>
		</tr>
	</tbody>
</table>