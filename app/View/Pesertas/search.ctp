<style type="text/css">
    #tombol{
        float:left;
        margin: -10px 0px 20px 0px;
    }

    .table-condensed>tbody>tr>th, .table-condensed>tbody>tr>td{
        text-align: center;
        vertical-align: middle;
    }
</style>

<table>
    <tr>
        <td class="col-md-7" style="border-bottom:none; padding-left:0px;"><h2>Daftar Peserta OKA 2017</h2></td>
        <td style="border-bottom:none;">
            <?php echo $this->Form->create('Peserta',array('action' => 'search'));  ?>
            <td class="col-md-3" style='padding-right:0px; padding-top:27px; border-bottom:none'>
                <?php 
                    echo $this->Form->input('Peserta.search',array('label' => false,'class'=>'form-control','placeholder'=>'Masukkan kata kunci'));
                 ?>
            </td>
            <td class="col-md-2" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->input('Peserta.dasar',array('label' => false,'class'=>'form-control','options'=>array('Peserta.nama'=>'Nama Peserta','Prodi.nama_prodi'=>'Program Studi'))); ?>
            </td>
            <td class="col-md-1" style='padding-right:0px; padding-left:0px; padding-top:27px; border-bottom:none'>
                <?php echo $this->Form->button('Cari',array('class'=>'btn btn-default')); ?>
            </td>
        </td>
    </tr>
</table>

<div id="tombol">
    <button class="btn btn-default"><?php echo $this->Html->link('Download .xls',array('controller'=>'Pesertas','action'=>'download'),array('target' => '_blank')); ?></button>
    <button class="btn btn-default"><?php echo $this->Html->link('Tambah data peserta',array('controller'=>'Pesertas','action'=>'add'),array('target' => '_blank')); ?></button>
</div>

<?php
    if(!empty($pesertas)){
?>
<table class="table table-bordered table-hover table-condensed">
    <tr>
        <th><?php echo $this->Paginator->sort('id','ID'); ?></th>
        <th><?php echo $this->Paginator->sort('nama','Nama'); ?></th>
        <th><?php echo $this->Paginator->sort('jenis_kelamin','Jenis Kelamin'); ?></th>
        <th><?php echo $this->Paginator->sort('prodi','Prodi'); ?></th>
        <th><?php echo $this->Paginator->sort('nim', 'NIM'); ?></th>
        <th><?php echo $this->Paginator->sort('created', 'Tanggal Pendaftaran')?></th>
        <th>Actions</th>
    </tr>
    <?php foreach ($pesertas as $peserta): ?>
    <tr>
        <td><?php echo $peserta['Peserta']['id']; ?></td>        
        <td><?php echo $this->Html->link($peserta['Peserta']['nama'], 
                                         array(
                                             'controller' => 'Pesertas', 
                                             'action' => 'detail', 
                                             $peserta['Peserta']['id']
                                         )
                                        ); 
            ?>
        </td>
        <td><?php echo $peserta['Peserta']['jenis_kelamin']; ?></td>
        <td><?php echo $peserta['Prodi']['nama_prodi']; ?></td>
        <td><?php echo $peserta['Peserta']['nim'] ?></td>
        <td><?php echo $peserta['Peserta']['created'] ?></td>
        <td>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Pesertas','action'=>'edit', $peserta['Peserta']['id']))?>'">Edit</button>
            <button class="btn btn-default" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'Pesertas','action'=>'delete', $peserta['Peserta']['id']),array('confirm' => 'Apakah anda yakin akan menghapus data ini?'))?>'">Delete</button>
        </td>
    </tr>
    <?php 
        endforeach;
        unset($pesertas);
    ?>
</table>
<div>
    <?php 
        echo $this->Paginator->first('First');
        echo " ";    
        
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev('<<');
        }

        echo " ";

        echo $this->Paginator->numbers(array('modulus' => 2));
        echo " ";

        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next('>>');
        }
        echo " ";
    
        echo $this->Paginator->last('Last');
    ?>
</div>
<?php
    }
    else{
        echo "Data peserta tidak ditemukan";
    }
?>


