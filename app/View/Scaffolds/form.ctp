<?php if ($this->request->action !== 'add'): ?>
		<?php echo $this->Form->postLink(
			__d('cake', 'Delete'),
			array('action' => 'delete', $this->Form->value($modelClass . '.' . $primaryKey)),
			array(),
			__d('cake', 'Are you sure you want to delete # %s?', $this->Form->value($modelClass . '.' . $primaryKey)));
		?>
<?php endif; ?>
<?php echo $this->Form->create('Peserta', array('action'=>'add')); ?>
<?php echo $this->Form->input('nama', array('label' => 'Nama Lengkap')); ?>
<?php echo $this->Form->input('jenis_kelamin',
                              array(
                                    'type' => 'radio', 
                                    'options' => array('Laki-laki' => 'Laki-Laki', 
                                                       'Perempuan' => 'Perempuan')
                                    )
                             ); 
?>
<?php echo $this->Form->input('tanggal_lahir', array(
                                                    'label' => 'Tanggal Lahir', 
                                                    'dateFormat' => 'DMY',
                                                    'minYear' => date('Y') - 70,
                                                    'maxYear' => date('Y') - 10 )); 
?>
<?php echo $this->Form->input('tempat_lahir', array('label' => 'Kota Kelahiran'));?>
<?php echo $this->Form->input('golongan_darah',
                              array(
                                    'type' => 'radio', 
                                    'options' => array('A' => 'A', 'B' => 'B', 'AB' => 'AB', 'O' => 'O')
                                    )
                             ); 
?>
<?php echo $this->Form->input('agama',
                              array(
                                    'type' => 'radio', 
                                    'options' => array('Buddha' => 'Buddha', 
                                                       'Hindu' => 'Hindu', 
                                                       'Islam' => 'Islam', 
                                                       'Katholik' => 'Katholik', 
                                                       'Kristen' => 'Kristen')
                                    )
                             ); ?>
<?php echo $this->Form->input('nomor_telpon', array('label'=>'Nomor Telepon yang bisa dihubungi')); ?>
<?php echo $this->Form->input('nomor_orangtua', array('label'=>'Nomor Telepon Orang Tua yang bisa dihubungi')); ?> 
<?php echo $this->Form->input('alamat_asal', array('label'=>'Alamat asal')); ?>
<?php echo $this->Form->input('alamat_jogja', array('label'=>'Alamat tempat tinggal di Yogyakarta')); ?>
<?php echo $this->Form->input('asal_sekolah', array('label'=>'Asal SMA/SMK')); ?>
<?php echo $this->Form->input('kota_asal_sekolah', array('label'=>'Kota Asal SMA/SMK')); ?>
<?php echo $this->Form->input('prodi', 
                              array(
                                  'label'=>'Program Studi', 
                                  'options' => array(
//                                          yang masuk database => yang ditampilkan
                                      'Teknik Informatika' => 'Teknik Informatika',
                                      'Sistem Informasi' => 'Sistem Informasi'
                                  )                                      
                              )
                             );
?>
<?php echo $this->Form->input('vegetarian', array(                                                    
                                                'type' => 'radio', 
                                                'options' => array('Vegan1' => 'Vegan1',
                                                                   'Vegan2' => 'Vegan2', 
                                                                   'Vegan3' => 'Vegan3',
                                                                   'Vegan4' => 'Vegan4')
                                                )
                             ); ?>
<?php echo $this->Form->input('alergi_makanan', array('label'=>'Alergi Makanan')); ?>
<?php echo $this->Form->input('alergi', array('label'=>'Alergi yang lain')); ?>
<?php echo $this->Form->input('penyakit_diderita', array('label'=>'Penyakit yang pernah atau sedang diderita')); ?>
<?php echo $this->Form->input('ukuran_almamater', 
                              array(
                                  'type' => 'radio', 
                                  'options' => array('S' => 'S', 
                                                     'M' => 'M', 
                                                     'L' => 'L', 
                                                     'XL' => 'XL', 
                                                     'XXL' => 'XXL', 
                                                    'lainya' => 'lainya')
                              )
                             ); ?>
<?php echo $this->Form->end('Selesai'); 
?>