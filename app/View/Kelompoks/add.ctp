<style type="text/css">
	h2{
		float:left;
	}
	hr{
		clear:both;
	}
	.tombolkembali{
		float:right;
		position: relative;
		top:24px;
	}
	.table-borderless td{ 
	    border: 0 !important;
	}
	button{
		float:right;
	}
	.table tbody tr{
    	background-color: white;
    }
	.table>tbody>tr>td.labelin{
	    font-weight: bold;
	    text-align: right;
	    padding-right: 20px;
	    vertical-align: middle;
	}
</style>

<h2>Tambah Data Kelompok OKA 2017</h2>
<button class="btn btn-primary tombolkembali" type="button" onclick="window.location.href='<?php echo Router::url(array('controller'=>'kelompoks','action'=>'index'))?>'">Kembali</button>
<hr>
<table>
<?php echo $this->Form->create('Kelompok', array('controller'=>'kelompoks','action'=>'add', 'inputDefaults'=>array('class'=>'form-control','label'=>false))); ?>
<table class="table table-hover table-borderless">
	<tbody>
		<tr><td class="col-md-2 labelin">Nama Kelompok</td>
			<td><?php echo $this->Form->input('nama_kelompok',array('div'=>false)); ?></td>
		</tr>
		<tr><td class="labelin">Nama Mapen</td>
			<td><?php echo $this->Form->input('nama_mapen',array('div'=>false)); ?></td>
		</tr>
	</tbody>
</table>
<?php echo $this->Form->button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;&nbsp;Selesai', array('class'=>'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>