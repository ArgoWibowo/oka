<?php
    App::uses('AppController', 'Controller');

	class ProdisController extends AppController{
		public $theme = "temautama";
		public $layout = "layout1";
		public $uses = array('Prodi','Peserta');

		public function index(){
			$this->set('title','Daftar Prodi UKDW');

            $this->paginate = array(
                'limit' => 20
            );
            
            $this->set('prodis', $this->paginate('Prodi'));
        }

        public function hitungAnggota($id=null){
            $anggotas = $this->Peserta->Find('all',array('conditions'=>array('Peserta.idprodi'=>$id)));
            $jumlah=array('laki-laki'=>'0', 'perempuan'=>'0');
            foreach ($anggotas as $anggota):
                if(strtolower($anggota['Peserta']['jenis_kelamin'])==='laki-laki'){
                    $jumlah['laki-laki'] += 1;
                }
                else if(strtolower($anggota['Peserta']['jenis_kelamin'])==='perempuan'){
                    $jumlah['perempuan'] += 1;
                }
            
            endforeach;
            unset($anggotas);
            return $jumlah;
        }

		public function add(){
        	 $this->set('title','Tambah Data Prodi UKDW');
            if ($this->request->is('post')) {
            	$this->Prodi->create();
                if ($this->Prodi->save($this->request->data)) {
                    	//debug($this->request->data);
                        $this->Session->setFlash(__('Prodi berhasil ditambahkan.'), 'default', array('class' => 'success'));
                        $this->redirect(array('controller'=>'Prodis','action' => 'add'));
                    } 
                else{
                	//debug($this->request->data);
                	$this->Session->setFlash(__('Prodi tidak berhasil ditambahkan.'));
                	$this->redirect(array('controller'=>'Prodis','action' => 'add'));
                }             
            }
        }

		public function edit($id=null){
            $this->set('title','Edit Data Prodi UKDW');
            $this->Prodi->id=$id;

            if ($this->request->is('post')) { 
                 if ($this->Prodi->save($this->request->data)) {
                    $this->Session->setFlash(__('Prodi berhasil diubah.'), 'default', array('class' => 'success'));
                    return $this->redirect(array('controller'=>'Prodis','action' => 'index'));
                }              
                $this->Session->setFlash(__('Prodi tidak berhasil diubah.'));
                return $this->redirect(array('controller'=>'Prodis','action' => 'index'));
            }
            else {
                if($id)
                {   
                    //TRY CATCH --------------------------------------
                    try
                    {
                        $data = $this->Prodi->read(null, $id);
                        //supaya saat edit ada teks sebelumnya
                        $this->request->data = $data;   
                    }
                    catch (NotFoundException $ex)
                    {
                        //NOTIFIKASI GAGAL
                        $this->Session->setFlash('Permintaan tidak valid');
                        $this->redirect(array('controller'=>'Prodis','action'=>'index'));    
                    }
                    //------------------------------------------------
                }
                //jika tdk post atau data tdk terisi, cek ada tidak data parameter
                else
                {
                    //jk tdk ada data parameter kasi NOTIFIKASI GAGAL
                    $this->Session->setFlash('Prodi tidak ada.');
                    $this->redirect(array('controller'=>'Prodis','action'=>'index'));
                } 
            }
		}

		public function delete($id=null){
            $this->request->onlyAllow('post');
            $this->Prodi->id = $id;
            
            if (!$this->Prodi->exists()) {
                throw new NotFoundException(__('Prodi tidak ada.'));
            }
            
            if ($this->Prodi->delete()) {
                $this->Session->setFlash('Prodi berhasil dihapus.');
                return $this->redirect(array('controller'=>'Prodis','action' => 'index')); 
            }
            
            $this->Session->setFlash('Prodi tidak berhasil dihapus.');
            return $this->redirect(array('controller'=>'Prodis','action' => 'index')); 
		}

        public function search(){
            $this->set('title','Hasil Pencarian Prodi');
            if (!empty($this->data)) {
                $searchstr = $this->data['Prodi']['search']; 
                $this->set('searchstring', $this->data['Prodi']['search']); 
                $this->paginate = array(
                    'limit'=> 20,
                    'conditions' => array( 
                        'or' => array(  
                            $this->data['Prodi']['dasar']." LIKE" => "%$searchstr%"   
                        ) 
                    ) 
                ); 
                $this->set('prodis', $this->paginate('Prodi')); 
            }
        }
	}
?>