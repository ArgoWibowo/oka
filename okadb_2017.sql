-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2017 at 10:01 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `okadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cds`
--

CREATE TABLE `cds` (
  `titel` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `interpret` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `jahr` int(11) DEFAULT NULL,
  `id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `cds`
--

INSERT INTO `cds` (`titel`, `interpret`, `jahr`, `id`) VALUES
('Beauty', 'Ryuichi Sakamoto', 1990, 1),
('Goodbye Country (Hello Nightclub)', 'Groove Armada', 2001, 4),
('Glee', 'Bran Van 3000', 1997, 5);

-- --------------------------------------------------------

--
-- Table structure for table `kelompoks`
--

CREATE TABLE `kelompoks` (
  `id` int(11) NOT NULL,
  `nama_kelompok` varchar(512) NOT NULL,
  `nama_mapen` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelompoks`
--

INSERT INTO `kelompoks` (`id`, `nama_kelompok`, `nama_mapen`) VALUES
(1, '001 Loncat Indah', 'Arka / Fiorentina'),
(2, '002 Dayung', 'Evan / Yohana Felisbella'),
(3, '003 Tolak Peluru', 'Daniel Kristianto / Frelagia Romana'),
(4, '004 Gulat', 'Yoshua Hendra / Karina Andhanary'),
(5, '005 Berkuda', 'Philiakishi Bilha / Julia Elfreda'),
(6, '006 Hoki', 'Edward Lipiston / Julyandani Pratiwi'),
(7, '007 Lompat Galah', 'Yoga Tirta / Ravaela Amba'),
(8, '008 Angkat Besi', 'Yoshua Stefan / Devi Kristina'),
(9, '009 Tenis Meja', 'Albertus Michael / Basillia Juventia'),
(10, '010 Rugby', 'Kenneth Vriezen / Ivana Kezia'),
(11, '011 Tinju', 'Imanuel Aji / Dea Inanditya'),
(12, '012 Bola Voli', 'Titus Rangga / Dorena Abigail'),
(13, '013 Golf', 'Daniel Cientifica / Nugradhyani Jwalita'),
(14, '014 Bola Tangan', 'Aswin Wahyudi / Rachel Teodhora'),
(15, '015 Senam', 'Axel Manuhutu / Clarissa Rante'),
(16, '016 Renang', 'Silvanus Santo / Stephanie Maria'),
(17, '017 Sepeda', 'Gamalael Wahyu / Priska Sahanaya'),
(18, '018 Lompat Jauh', 'Oktavianus Bima / Marlin Josefina'),
(19, '019 Lari Estafet', 'Arnaldo Adi / Galuh Gita'),
(20, '020 Sepak Bola', 'Yoas Maasea / Widyastuti Renaningsih'),
(21, '021 Lempar Lembing', 'Isaias Stany / Olivia Citra'),
(22, '022 Taekwondo', 'Rizky Fernando / Cyntia Ayu'),
(23, '023 Bola Basket', 'Calvin Junus / Anggun Filia'),
(24, '024 Lompat Tinggi', 'Michael Arung / Lisnawaty Saragih'),
(25, '025 Menembak', 'Aditya Fajar / Yoseptina Enggal'),
(26, '026 Kano', 'Ryan Meok / Cherry Diva'),
(27, '027 Tenis', 'Gerry Herbiyan / Grace Florensia'),
(28, '028 Bulu Tangkis', 'Ruddy Cahyanto / Maria Indriyanti'),
(29, '029 Baseball', 'Yosi Santoso / Tabita Febriawaty'),
(30, '030 Angkat Beban', 'Jonathan Tito / Yessy Meilita'),
(31, '031 Layar', 'Alexander Christian / Agnes Lestary'),
(32, '032 Anggar', 'Samuel Reynaldi / Elvirda Ibriani'),
(33, '033 Polo Air', 'Aldi Ardianto / Leo Vina'),
(34, '034 Judo', 'Ogy Septiadi / Felicia Agnes ');

-- --------------------------------------------------------

--
-- Table structure for table `pesertas`
--

CREATE TABLE `pesertas` (
  `id` int(4) NOT NULL,
  `nim` char(8) DEFAULT NULL,
  `idkelompok` int(2) DEFAULT NULL,
  `nama` varchar(200) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `golongan_darah` varchar(2) NOT NULL,
  `agama` varchar(9) NOT NULL,
  `nomor_telpon` varchar(20) NOT NULL,
  `nomor_orangtua` varchar(20) NOT NULL,
  `alamat_asal` varchar(300) NOT NULL,
  `alamat_jogja` varchar(300) NOT NULL,
  `asal_sekolah` varchar(100) NOT NULL,
  `kota_asal_sekolah` varchar(50) NOT NULL,
  `idprodi` int(2) NOT NULL,
  `ukuran_almamater` varchar(8) NOT NULL,
  `jenis_penyakit` enum('Penyakit Berat','Penyakit Ringan','Tidak Ada Penyakit') NOT NULL DEFAULT 'Tidak Ada Penyakit',
  `alergi` varchar(100) DEFAULT NULL,
  `penyakit_diderita` varchar(100) DEFAULT NULL,
  `alergi_makanan` varchar(100) DEFAULT NULL,
  `ralergi` varchar(200) NOT NULL,
  `rpenyakit` varchar(200) NOT NULL,
  `obat` varchar(100) NOT NULL,
  `tonti` char(5) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesertas`
--

INSERT INTO `pesertas` (`id`, `nim`, `idkelompok`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `golongan_darah`, `agama`, `nomor_telpon`, `nomor_orangtua`, `alamat_asal`, `alamat_jogja`, `asal_sekolah`, `kota_asal_sekolah`, `idprodi`, `ukuran_almamater`, `jenis_penyakit`, `alergi`, `penyakit_diderita`, `alergi_makanan`, `ralergi`, `rpenyakit`, `obat`, `tonti`, `created`, `modified`) VALUES
(1, '11160011', 6, 'DEFIYANTO PABIDANG', 'Laki-laki', 'RANTEPAO', '1998-09-22', '', 'Kristen', '085242455570', '081342007771', 'JALAN DIPONEGORO NO. 48 ', 'JALAN KUSBINI', 'SMA NEGERI 2 RANTEPAO', 'TORAJA UTARA', 8, 'L', 'Penyakit Berat', '-', 'PARU-PARU', '-', '-', 'PUSING', 'BODREX', 'Tidak', '2016-06-06 08:37:54', '2017-05-29 14:50:06'),
(2, '61150019', 10, 'CAROLUS ERSAN KURNIA PANGGUR', 'Laki-laki', 'YOGYAKARTA', '1997-11-04', '', 'Katholik', '082358874301', '081392617350', 'JL.DAMANHURI.PERUM.ARTAS.BLOK.BQ.NO.06.SAMARINDA.KALIMANTAN TIMUR', 'NOLOGATEN.JL.KAPULOGO.NO.242A', 'SMKN 6 SAMARINDA', 'SAMARINDA', 6, 'empty', 'Tidak Ada Penyakit', '-', '-', 'NASI,IKAN LAUT', '-', '-', '-', 'Tidak', '2016-06-06 08:58:05', '2016-06-06 15:10:23'),
(3, '61160006', 11, 'ERIZA AGUSTIN DAPASRIANDI ', 'Laki-laki', 'SEKOLAQ ODAY ', '1998-08-29', 'B', 'Kristen', '081258021129', '08115803831', 'ROYOQ, JALAN SENDAWAR RAYA RT 03 KUTAI BARAT KALIMANTAN TIMUR', 'JALAN. SAMIRONO CT VII KELURAHAN CATURTUNGGAL KECAMATAN DEPOK KABUPATEN SLEMAN KOTA YOGYAKARTA', 'SMA NEGERI 1 SENDAWAR', 'KUTAI BARAT ', 6, 'M', 'Tidak Ada Penyakit', '-', '-', 'MSG ', '-', '-', '-', 'Tidak', '2016-06-06 09:00:36', '2016-06-06 15:10:34'),
(4, '62160001', 16, 'NOVIA ADITYA MANDALA', 'Perempuan', 'BANJARMASIN', '1997-11-29', 'O', 'Kristen', '6287814003990', '62811510861', 'JALAN A.YANI, CITRALAND CLUSTER THE SIGN B6-15, KABUPATEN BANJAR, KALIMANTAN SELATAN', 'JALAN GEJAYAN KARANGASEM KOST D\'PARAGON', 'SMAK KANAAN BANJARMASIN', 'BANJARMASIN', 5, 'M', 'Tidak Ada Penyakit', '-', 'MAAG, DISLOKASI', '-', '-', 'SAKIT PERUT, SAKIT DI KAKI', '-', 'Ya', '2016-06-06 09:26:52', '2016-06-06 15:12:15'),
(5, '41160020', 12, 'VACE LIANSIA', 'Perempuan', 'SINGKAWANG', '1997-04-12', 'B', 'Katholik', '6285246940333', '6281352232660', 'JL. MASUKA DARAT GG ANEKA 2 NO.12 RT11/RW01 SINTANG. KALIMANTAN BARAT', 'KLITREN LOR GK3.423', 'SMA NEGERI 3 SINTANG', 'SINTANG, KALIMANTAN BARAT', 10, 'S', 'Tidak Ada Penyakit', '-', 'MIGRAIN', '-', '-', 'SAKIT KEPALA SEBELAH', 'PONCOFEN', 'Tidak', '2016-06-06 09:40:47', '2016-06-06 15:09:45'),
(6, '71160023', 17, 'OWEN ALOYSIUS', 'Laki-laki', 'PALEMBANG', '1999-10-01', 'O', 'Budha', '6289602199184', '6285267626722', 'JALAN ARJUNA RAYA BLOK J1 NO 6 SAKO KENTEN, PALEMBANG 30163', 'JALAN BALAPAN NO 5 RT 62 RW 16 KECAMATAN GONDOKUSUMAN KELURAHAN KLITREN YOGYAKARTA', 'SMA XAVERIUS 1 PALEMBANG', 'PALEMBANG', 1, 'M', 'Tidak Ada Penyakit', 'DEBU', 'ASMA', '-', 'SESAK', 'SESAK', 'OBAT BATUK', 'Ya', '2016-06-06 09:57:35', '2016-06-06 15:12:59'),
(7, '72160010', 19, 'PEDRO RAYMON LAPEBESI', 'Laki-laki', 'KUPANG', '1998-05-12', 'A', 'Kristen', '6282247202635', '6281386432965', 'JL. FRANS SEDA/RT.029/RW.009/KEL.FATULULI/KEC.OEBOBO', '-', 'SMA KRISTEN MERCUSUAR', 'KUPANG', 2, 'S', 'Tidak Ada Penyakit', '-', 'KELOID', 'SEAFOOD', 'GATAL-GATAL', 'GATAL-GATAL', '-', 'Tidak', '2016-06-06 10:01:05', '2016-06-06 15:14:24'),
(8, '61160017', 7, 'CINDY PRISCILLIA SINAY', 'Perempuan', 'MERAUKE', '1998-06-24', 'O', 'Kristen', '082398711809', '081343181807', 'JLN GANG PENDIDIKAN MERAUKE, PAPUA', 'JLN KUMANDAMAN', 'SMA YPPK YOANES XXIII MERAUKE', 'MERAUKE', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-06 10:11:33', '2016-06-24 11:41:35'),
(9, '12160011', 3, 'DWI AYU MAHARANI LENA RADJA', 'Perempuan', 'WAIKABUBAK', '1998-06-10', 'O', 'Kristen', '6282247711207', '6282327322294', 'JL, ADYAKSA KM 3 WAIKABUBAK SUMBA BARAT NTT', 'BABARSARI TB 12 NO. 6B', 'SMA NEGERI 1 WAIKABUBAK', 'WAIKABUBAK', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 10:23:48', '2016-06-06 15:07:32'),
(10, '61160018', 14, 'JORFARICIO RUSLIANO CARVEIRO LOPES', 'Laki-laki', 'PUNO', '1996-02-10', '', 'Katholik', '6281238786117', '62882144224437', 'PAIRARA', 'KLITREN LOR GK 3 NO.246', 'STM LOSPALOS', 'LOSPALOS', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 10:30:00', '2016-06-06 15:10:43'),
(11, '61150106', 4, 'IKA ADIENI', 'Perempuan', 'SINGKAWANG', '1997-12-30', 'B', 'Katholik', '081348545097', '085347812079', 'JL.KESATRIAAN NO 78 SINGKAWANG, KALIMANTAN BARAT', 'JL.MAGELANG NO 792 , KEL.KRICAK KEC.TEGALREJO', 'SMA SANTO IGNASIUS SINGKAWANG', 'SINGKAWANG, KALIMANTAN BARAT', 6, 'empty', 'Tidak Ada Penyakit', '-', 'MAGH, SESAK NAFAS', 'UDANG', 'GATAL - GATAL', 'SESAK', 'PROMAG, BIROTEX', 'Ya', '2016-06-06 10:38:31', '2016-06-06 15:11:38'),
(12, '11160012', 1, 'ETHA MARTHINA KAFIAR', 'Perempuan', 'SERUI', '1998-09-08', 'O', 'Kristen', '081296836252', '08124061134', 'KAMPUNG PARAY', 'JL KUSUMANEGARA', 'SMA SANTO MIKAEL', 'SLEMAN', 8, 'XL', 'Tidak Ada Penyakit', '-\r\n', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 10:44:42', '2016-06-06 15:05:37'),
(13, '71160025', 18, 'GABRIEL KADANG', 'Laki-laki', 'TEMBAGAPURA PAPUA', '1997-10-16', 'B', 'Kristen', '085218094168', '0811497586', 'KUALA KENCANA RT 3 NO 57 TIMIKA PAPUA', 'JLN STM PEMBANGUNAN NO 23', 'SMA BOPKRI 2 YOGYAKARTA', 'YOGYAKARTA', 1, 'XL', 'Tidak Ada Penyakit', '-', '-', 'MAKANAN LAUT', 'GATAL-GATAL', '-', '-', 'Tidak', '2016-06-06 11:10:34', '2016-06-06 15:13:15'),
(14, '61160019', 15, 'ARMANDO REYAAN', 'Laki-laki', 'AGATS', '1995-03-13', 'B', 'Katholik', '081344426033', '081225907329', 'JL. GUDANG ARANG ', 'JL. SORANGAN', 'SMK NEGERI 3 MERAUKE', 'MERAUKE', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 11:25:46', '2016-06-06 15:11:01'),
(15, '11160013', 2, 'FRANSISKA APRILINI YONA DATUS', 'Perempuan', 'RUTENG', '1998-04-10', 'A', 'Katholik', '081338285364', '081236055413', 'JALAN ULUMBU -KAMPUNG MAUMERE', 'JALAN SAGAN', 'SMAK SETIA BAKTI RUTENG', 'RUTENG', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-06 11:33:17', '2016-06-06 15:06:01'),
(16, '11150137', 9, 'DUBALANG IGRAHA SOLIN', 'Laki-laki', 'SIDIKALANG', '1996-11-07', 'O', 'Kristen', '085277877269', '085262377644', 'JALAN PEMUDA NO 29A SIDIKALANG, KABUPATEN DAIRI,SUMATERA UTARA', 'JALAN KLITREN LOR GK III', 'SMA NEGERI 1 SIDIKALANG', 'SIDIKALANG, KABUPATEN DAIRI, SUMATERA UTARA', 8, 'empty', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-06 11:47:29', '2016-06-06 15:06:43'),
(17, '31160013', 8, 'JESSICA LEONI', 'Perempuan', 'YOGYAKARTA', '1998-08-17', 'O', 'Katholik', '081578272235', '08882820401', 'JL.PARANGTRITIS NO:10 YOGYAKARTA', 'JL.PARANGTRITIS NO.10 YOGYAKARTA', 'SMA STELLA DUCE 1 YOGYAKARTA', 'YOGYAKARTA', 3, 'S', 'Tidak Ada Penyakit', 'ASAP ROKOK', 'VERTIGO,ANEMIA', 'CUMI-CUMI', 'GATEL-GATEL,SESAK NAFAS', 'PUSING', 'SANGOBION DAN RESEP DOKTER', 'Ya', '2016-06-06 11:58:43', '2016-06-06 15:09:22'),
(18, '11160014', 5, 'VINSENSIUS PALEM', 'Laki-laki', 'TANJUNG PINANG', '1998-04-05', '', 'Katholik', '085668524379', '081364388395', 'JLN.GATOT SUBROTO .NO 53', 'JLN.DEMANGAN BARU', 'SMK N 3.TANJUNGPINANG', 'TANJUNGPINANG', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', 'UDANG', 'UDANG=GATAL\r\nDEBU=GATAL', '-', '-', 'Ya', '2016-06-06 12:10:19', '2016-06-06 15:06:52'),
(19, '71160031', 12, 'ADRIAN BOBANE', 'Laki-laki', 'ARU IRIAN', '1997-08-31', 'O', 'Kristen', '082290805587', '082328999814', 'MALUKU UTARA', 'BALIREJO', 'SMA NEGERI 1 HALMAHERA UTARA', 'HALMAHERA UTARA', 1, 'L', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'SAKIT KEPALA', 'PROMAG', 'Ya', '2016-06-06 12:17:22', '2016-06-06 15:13:38'),
(20, '12160012', 13, 'ANGGIA RADI DADE', 'Perempuan', 'ENDE', '1998-07-16', 'A', 'Kristen', '082247192933', '082225226726', 'JLN MAKAM PAHLAWAN WAIKABUBAK SUMBA BARAT NTT', 'JL KILTREN LOR', 'SMA NEGERI 1 WAIKABUBAK', 'WAIKABUBAK', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-06 12:36:25', '2016-06-06 15:08:54'),
(21, '71160032', 8, 'I MADE JERRYCO BIGBRO ERMAN', 'Laki-laki', 'NEGARA', '1998-11-10', 'B', 'Kristen', '081238270010', '082237802828', 'JALAN PANTAI BALIAN GANG CEMPAKA NO.14', '-', 'SMK TI BALI GLOBAL DENPASAR', 'DENPASAR', 1, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 13:33:17', '2016-06-06 15:13:47'),
(22, '71160033', 4, 'IVAN SURYA WICAKSANA', 'Laki-laki', 'WONOSOBO', '1998-06-17', 'O', 'Kristen', '085643522859', '081328704459', 'JALAN MAYOR KASLAM NO 99 WONOSOBO', 'PURBONEGARAN GK V/1248', 'SMA NEGERI 2 WONOSOBO', 'WONOSOBO', 1, 'XXXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 14:20:28', '2016-06-06 15:14:01'),
(23, '01160001', 1, 'YEHEZKIEL DWI CAHYA YOGA RESPATI', 'Laki-laki', 'YOGYAKARTA', '1998-07-16', 'B', 'Kristen', '087739642002', '0817463508', 'JL.Gejayan Pelemkecut CT X / 48', 'JL.Gejayan Pelemkecut CT X / 48', 'SMA NEGERI 11 YOGYAKARTA', 'YOGYAKARTA', 4, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-06 14:34:00', '2016-06-06 15:04:13'),
(24, '12160003', 25, 'YESSICA PARAMITA JAYAPUTERA', 'Perempuan', 'YOGYAKARTA', '1998-06-26', 'B', 'Kristen', '082242020967', '081802668800', 'JL. CANGKRINGAN GRIYA AVIA CERIA JL. NURI NO.7 KALASAN', 'JL. CANGKRINGAN GRIYA AVIA CERIA JL. NURI NO.7 KALASAN', 'SMA STELLA DUCE 1', 'YOGYAKARTA', 7, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'BERSIN DAN FLU', '-', '-', 'Ya', '2016-06-07 10:04:08', '2016-06-07 15:06:39'),
(25, '72160012', 34, 'RICKY HENDZEN SINAGA', 'Laki-laki', 'SINGKAWANG', '1998-09-05', 'A', 'Kristen', '089675649453', '08215100240', 'JL. HANSIP GG. PENDIDIKAN NO.29 SINGKAWANG KALIMANTAN BARAT', 'JL.SAWA NO.106D KARANGGAYAM', 'SMA NEGERI 2 SINGKAWANG KALIMANGAN BARAT', 'SINGKAWANG', 2, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-07 10:30:56', '2016-06-07 15:09:40'),
(26, '72160005', 2, 'ANDREAS SAMPARI AIBEKOB', 'Laki-laki', 'TIMIKA ', '1997-08-27', 'O', 'Kristen', '6283199319631', '081248831121', 'TIMIKA JL.ELANG KWAMKILAMA', 'JL.BABARSARI NO.7', 'SMK KRISTEN 1 TOMOHON', 'TOMOHON', 2, 'M', 'Tidak Ada Penyakit', '-', 'MAAG', 'LOBSTER', '-', '-', '-', 'Ya', '2016-06-07 10:32:38', '2016-06-07 15:09:52'),
(27, '61160014', 31, 'FRANCO CHAROLS CH G WULUR ', 'Laki-laki', 'MANOKWARI ', '1999-03-24', 'O', 'Kristen', '081247455366', '081248804126', 'LINGKUNGAN 2 KAKASKASEN 2 KEC.TOMOHON UTARA KOTA TOMOHON. SULAWESI UTARA ', 'JALAN BABARSARI ', 'SMK KRISTEN 1 TOMOHON ', 'KOTA TOMOHON ', 6, 'XXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-07 10:41:46', '2016-06-07 15:08:39'),
(28, '12160013', 26, 'TANIA WAHYU WARDHANI', 'Perempuan', 'SAWAHLUNTO', '1998-06-19', 'O', 'Islam', '6281276424424', '6281226944408', 'SAWAHLUNTO SUMATERA BARAT', 'KALASAN YOGYAKARTA', 'SMA N 3', 'SOLOK', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-07 10:45:12', '2016-06-07 15:06:46'),
(29, '41160019', 29, 'NOVIANI MANDASAR', 'Perempuan', 'Temanggung', '1998-11-03', 'A', 'Kristen', '087834346020', '081328533955', 'Temanggung, Jalan Mujahidin gang vi rt 06 rw 04 no 4 giyanti', 'Kota baru jalan no 5', 'SMA N 1 Temanggung', 'Temanggung', 10, 'S', 'Tidak Ada Penyakit', 'Debu', '-', '-', 'Bersin', '-', '-', 'Tidak', '2016-06-07 10:59:53', '2016-06-09 09:24:47'),
(30, '71160006', 32, 'RYAN ARIESTILLMAN CHANDRA', 'Laki-laki', 'MUARA BUNGO', '1998-03-26', 'A', 'Budha', '081366662626', '081366456888', 'JL. MERDEKA NO. 21 MUARA BUNGO', 'JL. KRASAK BARAT NO. 31 KEC. GONDOKUSUMAN KOTA BARU JOGJAKARTA', 'SMA N 1 MUARA BUNGO', 'MUARA BUNGO', 1, 'XXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-07 11:08:16', '2016-06-07 15:09:14'),
(31, '01160002', 20, 'MARKUS PERDATA SEMBIRING', 'Laki-laki', 'BERASTAGI', '1998-03-24', 'B', 'Kristen', '6285760662179', '6285297056144', 'KUBU COLIA KECAMATAN DOLAT RAYAT KABUPATEN KARO PROVINSI SUMATRA UTARA', 'ASRAMA UKDW,SETURAN', 'SMA N 1 BARUS JAHE', 'KABUPATEN KARO', 4, 'L', 'Tidak Ada Penyakit', '-', '-', '\r\n-', '-', '-', '-', 'Ya', '2016-06-07 11:48:58', '2016-06-07 15:04:48'),
(32, '31160014', 28, 'GRACIELA CARINA NAJOAN', 'Perempuan', 'Tomohon', '1998-09-05', 'A', 'Kristen', '6281325332292', '6282134038797', 'Jln.Kalutay Kelurahan Kakaskasen Lingkungan 4 Kecamatan Tomohon utara Kota Tomohon', '-', 'SMA Kristen Satya Wacana ', 'Salatiga', 3, 'S', 'Tidak Ada Penyakit', '-', 'maag', '-', '-', 'pusing', 'PROMAAG', 'Ya', '2016-06-07 11:59:36', '2016-06-09 09:24:22'),
(33, '71160035', 33, 'VINCENCIO APPAULO RIVALDO BATA', 'Laki-laki', 'BAJAWA', '1998-09-23', 'O', 'Katholik', '082147590866', '081239422166', 'JL.AHMAD YANI - BAJAWA - FLORES', 'LEMPUYANGAN, KLITREN, YOGYAKARTA', 'SMA NEGERI 1 BAJAWA', 'BAJAWA', 1, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-07 12:04:31', '2016-06-07 15:09:20'),
(34, '01160004', 21, 'WANETRI IMANUELA BR SITEPU', 'Perempuan', 'SUKAJULU', '1998-09-22', 'B', 'Kristen', '6281375523553', '6282185646031', 'NO. 30 .SUKAJULU, KEC. BARUSJAHE, KAB. KARO, MEDAN, SUMATERA UTARA', '-', 'SMA N 1 BARUSJAHE', 'KABANJAHE', 4, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', 'KEPITING', 'GATAL-GATAL', '-', 'BEDAK ANTISEPTIK', 'Ya', '2016-06-07 12:44:20', '2016-06-07 15:05:02'),
(35, '12160015', 27, 'MELANCTON ANDRIAN BILI', 'Laki-laki', 'waikabubak', '1999-05-30', '', 'Kristen', '+6282147552715', '081246514785', 'JL. AHMAD YANI KM.1', '-', 'SMA NEGERI 1 WAIKABUBAK', 'WAIKABUBAK', 7, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-07 13:38:41', '2016-06-07 15:06:52'),
(36, '11160015', 24, 'WILLYAM RUMENGAN ', 'Laki-laki', 'wawondula', '1998-04-07', 'O', 'Kristen', '081355707344', '085342023097', 'jl.Danau tempe no 28', 'pengok', 'SMA YPS SOROWAKO', 'SOROWAKO', 8, 'S', 'Tidak Ada Penyakit', 'Alergi udara dingin.', '-', '-', 'gatal gatal', '-', 'FALERGI ', 'Ya', '2016-06-07 13:41:29', '2016-06-09 09:23:52'),
(37, '01160005', 22, 'MACA DINA VIRA TARIGAN', 'Perempuan', 'BALIKPAPAN', '1998-08-20', 'B', 'Kristen', '082254279838', '081350072699', 'JL.JEND SUDIRMAN PASAR BARU ASPOL WISMA SEGARA BLOK B.10 RT.28', '-', 'SMA NEGERI 5 BALIKPAPAN', 'BALIKPAPAN', 4, 'M', 'Tidak Ada Penyakit', '-', 'BATUK (JIKA MINUM ES), PILEK', '-', '-', '-', 'LINOS', 'Tidak', '2016-06-07 13:49:33', '2016-06-07 15:05:10'),
(38, '01160003', 23, 'Teguh Lamentur Takalapeta', 'Laki-laki', 'Kalabahi', '1998-12-17', 'O', 'Kristen', '6282236224242', '6285239345678', 'Jl. Samratulangi IV/III RT. 21 RW. 7 Kel. Oesapa Barat Kec. Kelapa Lima Kota Kupang NTT', '-', 'SMA N 3 KUPANG', 'KUPANG', 4, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-07 14:26:10', '2016-06-07 15:05:16'),
(39, '41160013', 30, 'FERDINANDO KENDEK', 'Laki-laki', 'NABIRE', '1995-12-07', 'B', 'Kristen', '6281344482981', '6281344668001', 'JL. BUKIT TASANGKAPURA HAMADI JAYAPURA', 'PERUM. CANDI GEBANG PERMAI BLOK D NO. 1 WEDOMARTANI NGEMPLAK SLEMAN', 'SMA NEGERI 4 JAYAPURA', 'JAYAPURA', 10, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-07 14:52:02', '2016-06-07 15:07:58'),
(40, '01160006', 3, 'KRISTIAN NANDA PRATAMA', 'Laki-laki', 'LAMPUNG', '1998-03-22', 'O', 'Kristen', '083840485348', '08179433804', 'NOGOSARI, SIDOKARTO 03/27 GODEAN SLEMAN ', 'JL RAYA NOGOTIRTO NO G-2, GUYANGAN, GAMPING, SLEMAN', 'SMA BOPKRI 2 YOGYAKARTA', 'YOGYAKARTA', 4, 'L', 'Tidak Ada Penyakit', 'DINGIN', 'MAAG, NYERI DILENGAN KIRI', '-', 'BINTIK -BINTIK MERAH GATAL', 'NYERI', 'PROMAG', 'Tidak', '2016-06-08 09:24:01', '2016-06-08 15:03:53'),
(41, '72160013', 22, 'HENDRICK ADI PRATAMA', 'Laki-laki', 'MAGELANG', '1997-12-30', 'O', 'Kristen', '085727428334', '085729412322', 'JL DAHA GANG LANGGAR NO. 348 MAGELANG', '', 'SMA TARAKANITA MAGELANG', 'MAGELANG', 2, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-08 09:26:41', '2016-06-08 15:08:40'),
(42, '11160016', 7, 'YEMICHO OKTAVISTA PANJAYA', 'Laki-laki', 'BATANG', '1995-10-06', 'B', 'Katholik', '087733465630', '081548055680', 'JL. BANDAR NO 189', '-', 'SMA SANTO BERNARDUS PEKALONGAN', 'PEKALONGAN', 8, 'L', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'PUSING MUAL', 'MILANTA', 'Ya', '2016-06-08 09:43:13', '2016-06-08 15:05:17'),
(43, '72140037', 25, 'TIMOTIUS RANGGA', 'Laki-laki', 'Sanggau ledo', '1996-03-30', '', 'Kristen', '082153773246', '085386930952', 'SANGGAU LEDO, KALIMANTAN BARAT', 'JL.KUSBINI NO.2', 'SMK 1 SANGGAU LEDO', 'BENGKAYANG', 2, 'empty', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-08 10:18:41', '2016-06-08 15:08:58'),
(44, '72160011', 11, 'MARGONIA LILA FATIMA PEREIRA', 'Perempuan', 'TIMOR-LESTE', '1997-01-29', 'AB', 'Katholik', '081337490183', '+67077896678', 'LOSPALOS,LAUTEM,TIOR-LESTE', 'JL. KLITREN LOR GK 321', 'NINO CONIS SANTANA', 'LOSPALOS', 2, 'S', 'Tidak Ada Penyakit', '-', '-', '--', '-', '-', '-', 'Ya', '2016-06-08 10:24:07', '2016-06-08 15:09:11'),
(45, '41160027', 6, 'DEBORA DESI SETIASARI', 'Perempuan', 'GROBOGAN', '1998-01-22', 'O', 'Kristen', '089617219747', '085290925350', 'SUGIHMANIK RT/RW 01/01 KEC. TANGGUNGHARJO KAB. GROBOGAN PROV. JAWA TENGAH', '-', 'SMA NEGERI 1 SALATIGA', 'SALATIGA', 10, 'S', 'Tidak Ada Penyakit', 'OBAT COTRIMOKSAZOL', 'TIFUS', '-', 'BEKAS LUKA HITAM', 'DEMAM, PUSING, MUAL', '-', 'Ya', '2016-06-08 10:34:07', '2016-06-08 15:07:08'),
(46, '11160017', 10, 'FANI JUISAN', 'Perempuan', 'GAMHOKU', '1998-05-03', 'A', 'Kristen', '085145957340', '081243069339', 'JL KM 10  KAO BARAT HALMAHERA UTARA', 'JL.MANGGIS NO 52', 'SMA KRISTEN TOBELO', 'TOBELO HALMAHERA UTARA', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'FLU', '-', 'INSANA', 'Tidak', '2016-06-08 11:19:01', '2016-06-08 15:05:40'),
(47, '61160021', 16, 'EDWIN EXCELL EFRUAN', 'Laki-laki', 'AMBON', '1999-08-29', '', 'Kristen', '082198088157', '081247124913', 'AMBON, HATU ', '-', 'SMA NEGERI 2 AMBON', 'AMBON', 6, 'S', 'Tidak Ada Penyakit', '-', 'PARU-PARU BASAH', '-', '-', 'BATUK', '-', 'Ya', '2016-06-08 11:25:30', '2016-06-08 15:07:34'),
(48, '62150006', 21, 'Y AVEN SANDY', 'Laki-laki', 'PRABUMULIH', '1996-05-05', 'O', 'Katholik', '081367541994', '085210196141', 'JALAN JENDRAL SUDIRMAN KM 6 CAMBAI PRABUMULIH SUMATRA SELATAN', 'JALAN SAGAN CT V NO. 105/37 DEPOK SLEMAN YOGYAKARTA RT 04 RW 02', 'SMA TEKNOLOGI NASIONAL PALEMBANG', 'PRABUMULIH', 5, 'empty', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'LAPAR, MUAL', 'PROMAG', 'Ya', '2016-06-08 11:42:08', '2016-06-08 15:07:58'),
(49, '11160018', 13, 'ANDRE TARIBUKA', 'Laki-laki', 'JAYAPURA', '1998-07-22', 'B', 'Kristen', '081247087487', '085244380643', 'PERUM.JAYA ASRI I N0 16', 'JL.MAWAR Gg.4 no. 80-81\r\nBACIRO,YOGYAKARTA', 'SMA NEGERI 4 JAYAPURA', 'JAYAPURA', 8, 'XL', 'Tidak Ada Penyakit', 'DEBU', '-', 'SEAFOOD', 'GATAL- GATAL, FLU', '-', '-', 'Tidak', '2016-06-08 13:21:45', '2016-06-08 15:05:58'),
(50, '01160007', 5, 'GRIFITH MERCIA', 'Perempuan', 'JAKARTA', '1998-08-04', 'O', 'Kristen', '081218956106', '08128415712', 'jALAN SWASEMBADA TIMUR 12 NO.8, TG. PRIOK, JAKARTA UTARA, DKI JAKARTA', '-', 'SMA NEGERI 80 JAKARTA UTARA', 'JAKARTA UTARA', 4, 'S', 'Tidak Ada Penyakit', '-', '-', 'UDANG, ', 'GATAL DAN BENGKAK PADA TANGAN', '-', 'HIGHBEE', 'Ya', '2016-06-08 13:39:18', '2016-06-08 15:04:06'),
(51, '12160016', 14, 'DESI MAGDALENA LAIMEHERIWA', 'Perempuan', 'KUPANG', '1996-12-11', 'O', 'Kristen', '085244941896', '082198684921', 'SIFNANA OMELE SAUMLAKI', 'JALAN WAHID HASYIM YOGYAKARTA', 'SMA UNGGULAN SAUMLAKI', 'SAUMLAKI', 7, 'S', 'Tidak Ada Penyakit', '-', 'MAAG,PUSING', '-', '-', 'LEMAS,PUSING', 'PROMAG', 'Ya', '2016-06-08 13:41:00', '2016-06-08 15:06:28'),
(52, '12160014', 23, 'SONIA PRAMUDYA JATI', 'Perempuan', 'Yogyakarta', '1998-07-10', 'O', 'Katholik', '089609297022', '085601020188', 'Jl. Prof. Dr. Soepomo UH4/1104 Yogyakarta', 'Jl. Prof. Dr. Soepomo UH4/1104 Yogyakarta', 'SMA STELLA DUCE 2 ', 'YOGYAKARTA', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-09 09:12:23', '2016-06-09 15:11:08'),
(53, '72160014', 24, 'LUSIA ODILLA FRIANTI', 'Perempuan', 'Cilacap', '1998-12-30', 'A', 'Katholik', '081354118586', '08124080488', 'Perumahan Puri Kelapa Gading1, Kav.2, Ganjuran Manukan, Condong Catur, Depok, Sleman', 'Perumahan Puri Kelapa Gading1, Kav.2, Ganjuran Manukan, Condong Catur, Depok, Sleman', 'SMA Stella Duce 2 Yogyakarta', 'YOGYAKARTA', 2, 'S', 'Tidak Ada Penyakit', 'DEBU, PANAS', 'ASMA, TIDAK BISA KECAPEKAN', '-', 'BATUK, BERSIN, PUSING', 'BATUK, SESAK NAFAS', 'VENTOLIN, OBAT PILEK', 'Ya', '2016-06-09 09:14:56', '2016-06-09 15:18:33'),
(54, '41160009', 20, 'AGATHA MAHESWARI ADITA PUTRI', 'Perempuan', 'YOGYAKARTA', '1999-01-25', 'O', 'Katholik', '6282134439161', '6285725755708', 'TUKANGAN DN 2/477 RT.25 RW.05 YOGYAKARTA', 'TUKANGAN DN 2/477 RT.25 RW.05 YOGYAKARTA', 'SMA N 3 YOGYAKARTA', 'YOGYAKARTA', 10, 'S', 'Tidak Ada Penyakit', '-', 'SINUSITIS', '-', '-', 'SESAK DAN PILEK', '-', 'Tidak', '2016-06-09 10:54:32', '2016-06-09 15:12:19'),
(55, '61160022', 29, 'YESHOAH ANTHONIO ARRANG PANGGALO', 'Laki-laki', 'SLEMAN', '1998-07-23', 'B', 'Kristen', '6281904258001', '6281215678513', 'SIDOREJO RT005 RW 009 HARGOBINANGUN PAKEM SLEMAN', 'SIDOREJO RT005 RW 009 HARGOBINANGUN PAKEM SLEMAN', 'SMK BOPKRI I', 'YOGYAKARTA', 6, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-09 11:18:58', '2016-06-09 15:16:07'),
(56, '12160018', 18, 'LOVELLY RAISA RAULE', 'Perempuan', 'MANADO', '1998-09-21', 'B', 'Kristen', '6282188711987', '6285299272697', 'PERUMAHAN WALE MANGUNI INDAH BLOK AI/4, SINGKIL, KOMBOS TIMUR, MANADO', 'JL. RESONEGARAN SAGAN KIDUL, GK. 5/1347, KEL. TERBAN, KEC. GONDOKUSUMAN, YOGYAKARTA', 'SMA KRISTEN EBEN HAEZAR', 'MANADO', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-09 11:37:55', '2016-06-09 15:11:18'),
(57, '11160019', 26, 'REINHART HERY WIJAYA', 'Laki-laki', 'PALU', '1998-10-25', '', 'Kristen', '6282136803056', '082290725000', 'BTELEME,MOROWALI SULAWESI TENGAH', 'JAMBUSARI', 'SMA BOPKRI 1 ', 'YOGYAKARTA', 8, 'XXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-09 11:48:43', '2016-06-09 15:10:35'),
(58, '41160016', 28, 'JOSHUA HARIARA', 'Laki-laki', 'JAKARTA', '1997-08-20', 'A', 'Kristen', '081228275461', '081393397677', 'JALAN SINGOSARI SELATAN 2 NOMOR 8 NUSUKAN BANJARSARI SURAKARTA', 'JALAN MAWAR BACIRO', 'SMA N 7 ', 'SURAKARTA', 10, 'XL', 'Tidak Ada Penyakit', '-', 'ASMA', '-', '-', 'SESAK NAFAS', 'VENTOLIN', 'Ya', '2016-06-09 12:19:29', '2016-06-09 15:12:33'),
(59, '11160022', 17, 'JESSYCKA VERONIKA FURE', 'Perempuan', 'TERNATE', '1999-04-13', '', 'Kristen', '085825155551', '085757150150', 'GOIN KEC. IBU UTARA KAB. HALMAHERA BARAT PROV. MALUKU UTARA', 'BERBAH', 'SMA KRISTEN EBEN HAEZER IBU', 'GOIN', 8, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'SESAK DADA', 'MILANTA', 'Ya', '2016-06-09 12:50:17', '2016-06-09 15:10:20'),
(60, '41160040', 27, 'TIARA ADELEDYA THESALONIKA KARWUR', 'Perempuan', 'PALU', '1999-04-29', '', 'Kristen', '085330856209', '082196660022', 'JALAN WOODWARD NO.445', 'JALAN PALAGAN TENTARA PELAJAR PERUMAHAN THE PARADISE KAVLING ROSEMARY A4', 'SMA N 1 ', 'PALU', 10, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'SAKIT PERUT', 'ANTASIDA', 'Tidak', '2016-06-09 12:54:12', '2016-06-09 15:15:05'),
(61, '61160025', 1, 'CESLAUS YOSEF KARO KAKI', 'Laki-laki', 'ENDE', '1998-07-17', 'O', 'Katholik', '082237480849', '081239386569', 'JL. UDAYANA ENDE, FLORES-NTT', 'JL LAKSDA ADISUCIPTO, GANG MERANTI, NO.135D', 'SMAK SYURADIKARA', 'ENDE', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-09 12:56:43', '2016-06-09 15:16:39'),
(62, '61160026', 2, 'JOEL ANTONIO DOS SANTOS MONIZ', 'Laki-laki', 'TOBUR', '1996-01-11', 'O', 'Katholik', '081239202130', '+67076333093', 'TIMOR LESTE', 'PINGIT JL.KYAI MOJO', 'SMA NEGRI 1 SUAI', 'SUAI', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-09 13:25:16', '2016-06-09 15:17:08'),
(63, '61160023', 3, 'BENEDICTUS JUAN PRATAMA', 'Laki-laki', 'JAKARTA', '1998-07-17', 'A', 'Katholik', '0817703100', '08567842222', 'JL. LESTARI GG. ALJAUHARI RT 11/ RW 03 NO. 72. KEC. KALISARI KEL. PASAR REBO. JAKARTA TIMUR', 'JL. IROMEJAN GK 3/ 774, KLITREN RT. 034 RW. 009, GONDO KUSUMAN. YOGYAKARTA', 'BUDHI WARMAN II', 'JAKARTA', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-09 13:25:24', '2016-06-09 15:17:23'),
(64, '01160008', 15, 'CINDY GLORI LASANDER', 'Perempuan', 'SORONG', '1998-03-25', 'O', 'Kristen', '6282291492197', '6281243288696', 'KELURAHAN BAHU, KECAMATAN SIAU TIMUR, KABUPATEN SIAU TAGULANDANG BIARO', 'IROMEJAN, GK 3', 'SMA NEGERI 1 SIAU TIMUR', 'ULU', 4, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-09 13:31:03', '2016-06-09 15:09:34'),
(65, '61160027', 9, 'ROS INDAH ASTRI HUTAGAOL', 'Perempuan', 'KEFAMENANU', '1998-07-30', 'A', 'Kristen', '6285333421426', '6285253474444', 'JALAN ELTARI KM. 7 SASI , KOTA KEFAMENANU', 'BABARSARI', 'SMA NEGERI 1 KEFAMENANU', 'KEFAMENANU', 6, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'PILEK, BERSIN.', '-', '-', 'Ya', '2016-06-09 13:35:41', '2016-06-09 15:17:38'),
(66, '12160017', 19, 'MARIA NORMALITA DA SILVA', 'Perempuan', 'LOSPALOS', '1996-08-30', 'O', 'Katholik', '6281327649682', '67077317878', 'LOSPALOS,LAUTEM,TIMOR LESTE', 'KLITREN LOR GKIII RW 03 RT 09 NO.347', 'NINO KONIS SANTANA', 'LOSPALOS', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-09 15:04:53', '2016-06-09 15:11:32'),
(67, '72160003', 13, 'DIMAS SANJAYA', 'Laki-laki', 'KLATEN', '1998-02-15', 'A', 'Kristen', '087803935023', '0272320320', 'JALAN KIOS PASAR SRAGO 52 KLATEN', '-', 'SMA NEGERI 1 KLATEN', 'KLATEN', 2, 'XL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 09:16:55', '2016-06-10 15:12:19'),
(68, '41160005', 26, 'ADITYA  JERRY DEODATUS', 'Laki-laki', 'DENPASAR', '1997-04-15', 'O', 'Kristen', '082247515359', '081337920940', 'PERUM PRISKILA NO 21 BR. TEGALJAYA', 'KALIJERUK WIDODOMARTANI NGEMPLAK SLEMAN', 'SMA NEGERI 1 KUTA UTARA', 'BADUNG', 10, 'XXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 09:23:26', '2016-06-10 15:08:14'),
(69, '41160032', 21, 'PUTU EVAN CAHYADI', 'Laki-laki', 'BADUNG', '1997-12-12', 'A', 'Kristen', '081238565735', '081353275678', 'BR. UMACANDI, BUDUK, MENGWI, BADUNG, BALI', 'KALIJERUK, WIDODOMARTINI, NGEMPLAK, SLEMAN', 'SMA NEGERI 1 KUTA UTARA', 'BADUNG', 10, 'XXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-10 09:28:08', '2016-06-10 15:08:30'),
(70, '71160036', 30, 'JOSHUA ANDREAN CAHYO SAPUTRA', 'Laki-laki', 'YOGYAKARTA', '1997-11-29', 'B', 'Katholik', '087739888195', '081904140888', 'BESKALAN GM 1/454', 'JL.IREDA.73, YOGYAKARTA', 'SMA BUDYA WACANA YOGYAKARTA', 'YOGYAKARTA', 1, 'XXXXL', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'BERSIN-BERSIN', '-', '-', 'Ya', '2016-06-10 09:46:52', '2016-06-10 15:10:59'),
(71, '61160010', 8, 'WILUAJENG PUTRI ERDINA', 'Perempuan', 'YOGYAKARTA', '1997-09-30', 'O', 'Katholik', '6285643234209', '628122702811', 'BANTENG BARU, BANTENG RAYA UTARA NO.3, NGAGLIK, SLEMAN', 'BANTENG BARU, BANTENG RAYA UTARA NO.3, NGAGLIK, SLEMAN', 'SMA STELLA DUCE 1 ', 'YOGYAKARTA', 6, 'M', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'MUAL', '-', 'Ya', '2016-06-10 10:25:39', '2016-06-10 15:09:04'),
(72, '11160020', 34, 'KARTONO', 'Laki-laki', 'KETAPANG', '1997-04-21', 'A', 'Katholik', '081257103325', '081345315139', 'JLN.R.SUPRAPTO NO187 KETAPANG', '-', 'SMA PL. ST YOHANES', 'KETAPANG', 8, 'M', 'Tidak Ada Penyakit', 'DEBU', 'ASMA', '-', 'PILEK', 'SESAK NAFAS', '-', 'Tidak', '2016-06-10 10:27:03', '2016-06-10 15:04:25'),
(73, '11160021', 30, 'ELISABET HANNI VALENSIA', 'Perempuan', 'PONTIANAK', '1997-10-17', 'O', 'Katholik', '628988105515', '6282357757730', 'JL. SUNGAI RAYA DALAM KOM.MITRA INDAH UTAMA 7 NO.B4 PONTIANAK', 'JL.URIP SUMOHARJO GANG NANAS', 'SMA GEMBALA BAIK', 'PONTIANAK', 8, 'S', 'Tidak Ada Penyakit', '-', 'PERNAH PATAH TULANG BANGIAN JAUH', '-', '-', 'NGGAK PERNAH KAMBUH', '-', 'Ya', '2016-06-10 10:32:22', '2016-06-10 15:04:01'),
(74, '72160001', 7, 'STEISY PUTRI UTAMI RAUN', 'Perempuan', 'KALUWATU', '1998-08-26', 'O', 'Kristen', '085340554755', '081244922247', 'SINGKIL DUA LINGKUNGAN III MANADO SULAWESI UTARA', '-', 'SMK YADIKA MANADO', 'MANADO', 2, 'S', 'Tidak Ada Penyakit', 'NYAMUK, DEBU', '-', 'BUAH NANAS', 'GATAL-GATAL, BERSIN', '-', '-', 'Tidak', '2016-06-10 10:32:37', '2016-06-10 15:12:25'),
(75, '61160028', 5, 'SEMARLA JELANI', 'Perempuan', 'RANTEPAO', '1998-03-09', 'O', 'Kristen', '082349004799', '081342557669', 'JALAN PONGTIKU LRG.2A NO. 6', '-', 'SAM NEGERI 2 RANTEPAO', 'RANTEPAO', 6, 'S', 'Tidak Ada Penyakit', '-', '-', 'UDANG', 'GATAL-GATAL', '-', '-', 'Ya', '2016-06-10 11:21:07', '2016-06-10 15:09:16'),
(76, '61160029', 25, 'NADILA PUTRI PATRICIA AIBINI', 'Perempuan', 'TIMIKA', '1998-06-27', 'O', 'Kristen', '6281311341376', '6281240092131', 'JLN.BANGAU NO 44 KUALA KENCANA TIMIKA PAPUA', 'JLN TAMBAK BAYAN 9 NO 1 CATUR TUNGGAL DEPOK SLEMAN', 'SMA STELLA DUCE 2', 'YOGYAKARTA', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-10 11:25:57', '2016-06-10 15:09:29'),
(77, '11160002', 31, 'LIDYA NATALIA', 'Perempuan', 'SURAKARTA', '1998-04-03', 'AB', 'Katholik', '081228115669', '082133800111', 'WURYANTORO KIDUL RT.3/RW.1', '-', 'SMA REGINA PACIS SURAKARTA', 'SURAKARTA', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 11:27:03', '2016-06-10 15:04:09'),
(78, '12160019', 33, 'DESY PERMATA SARY', 'Perempuan', 'SINTANG', '1998-04-08', 'B', 'Katholik', '6289693983125', '6285389433034', 'JL.KHAIRUL MULUK , GG.ANEKA BUAH ', 'JL.KLITREN LOR GK 3 NO.423', 'SMA PANCA SETYA ', 'SINTANG', 7, 'S', 'Tidak Ada Penyakit', '-', 'BEKAS PATAH KAKI', '-', '-', '-', '-', 'Ya', '2016-06-10 11:33:51', '2016-06-10 15:05:15'),
(79, '72160002', 14, 'WINDY PUJI OKTIAGRAHA', 'Laki-laki', 'GUNUNGKIDUL', '1997-10-19', 'A', 'Kristen', '6282138528587', '6281392613872', 'SUDIMORO RT 01/ RW 01, KELOR, KARANGMOJO, GUNUGKIDUL', '-', 'SMA 2 WONOSARI', 'WONOSARI', 2, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-10 11:35:55', '2016-06-10 15:12:14'),
(80, '31160015', 32, 'MARIA DOLOROSA E.BENGA LULI', 'Perempuan', 'DILI', '1997-09-15', 'B', 'Katholik', '081236404496', '085253449085', 'KOLIMASANG,FLORES TIMUR', 'PERUMAHAN PURWO ELOK NO.9', 'SMAK SYURADIKARA', 'ENDE', 3, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 11:45:37', '2016-06-10 15:07:08'),
(81, '31160016', 10, 'FISTA YOHANA TANAYA', 'Perempuan', 'SURAKARTA', '1998-06-26', 'A', 'Kristen', '085725335299', '08122651762', 'JL YOS SUDARSO NO 273 GEMBLEGAN SOLO', '-', 'SMA REGINA PACIS SURAKARTA', 'SURAKARTA', 3, 'M', 'Tidak Ada Penyakit', '-', 'TYPUS', '-', '-', '-', '-', 'Ya', '2016-06-10 11:56:14', '2016-06-10 15:07:22'),
(82, '71160037', 29, 'MARCELINO R WEMPI', 'Laki-laki', 'TERNATE', '1998-10-12', 'A', 'Kristen', '081291339104', '081247965156', 'BARU RT/RW:009/005 KEL/DESA :BARU KECAMATAN:IBU SELATAN', 'JL.MAGELANG KRICAK KIDUL', 'SMA KRISTEN TOBELO', 'TOBELO ', 1, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 12:02:29', '2016-06-10 15:11:33'),
(83, '12160020', 34, 'SANCE BADIANG KULAPE', 'Perempuan', 'DORO', '1997-03-05', 'O', 'Kristen', '082188185754', '082190099950', 'DORO DUSUN III HALMAHERA UTARA', 'JALAN MAGELANG KRICAK KIDUL', 'SMA KRISTEN TOBELO', 'TOBELO', 7, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'LEMAS', 'PROMAAG', 'Tidak', '2016-06-10 12:07:01', '2016-06-10 15:05:36'),
(84, '72160015', 12, 'HENRY KRISTIANTO', 'Laki-laki', 'Magelang', '1998-07-23', 'O', 'Katholik', '0895325946612', '081328790006', 'JL SUNAN BONANG 3 NO 9 MAGELANG', 'JL KLITREN', 'SMA TARAKANITA MAGELANG', 'MAGELANG', 2, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 12:14:34', '2016-06-10 15:12:12'),
(85, '31150036', 9, 'JUNENGSI CARLI DAHOKLORY', 'Perempuan', 'YAWURU', '1997-06-14', 'O', 'Kristen', '081344021080', '085200552069', 'MALUKU BARAT DAYA', 'KRASAK TIMUR', 'SMA NEGERI 1 PP TERSELATAN', 'MALUKU BARAT DAYA', 3, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'LEMAS, MUAL', 'DOMPERIDONE', 'Ya', '2016-06-10 12:59:20', '2016-06-10 15:07:34'),
(86, '12160021', 15, 'MARLINA MAKLONIA GINUNY', 'Perempuan', 'BINTUNI', '1998-03-15', 'A', 'Kristen', '082398571481', '082197512224', 'BINTUNI BARAT KM 3', 'JL. BAUSASRAN', 'SMK NEGERI 1 BINTUNI', 'BINTUNI PAPUA BARAT', 7, 'S', 'Tidak Ada Penyakit', '-', 'SESAK NAFAS', '-', '-', 'SESAK ', '-', 'Tidak', '2016-06-10 13:20:15', '2016-06-10 15:06:18'),
(87, '12160022', 20, 'MIKHA OCTAVIANUS PRANOTO', 'Laki-laki', 'SEMARANG', '1998-10-18', 'A', 'Kristen', '081329830075', '081329824386', 'JL ANGGREK 3/B1 SOLO BARU', '-', 'PELITA NUSANTARA KASIH', 'SURAKARTA', 7, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-10 14:10:18', '2016-06-10 15:06:32'),
(88, '11160025', 32, 'IKBAL POCAI RANTETANDUNG', 'Laki-laki', 'TONDON ', '1997-09-09', 'B', 'Kristen', '082291382227', '085145302555', 'TONDON MATALLO TORAJA UTARA SULAWESI SELATAN', '-', 'SMAN 2 RANTEPAO ', 'TORAJA', 8, 'L', 'Tidak Ada Penyakit', '-', 'FERTIGO', '-', '-', 'PUSING', '-', 'Tidak', '2016-06-10 14:25:04', '2016-06-10 15:04:40'),
(89, '61160031', 6, 'SET ADRIAN APONNO', 'Laki-laki', 'tual', '1998-11-27', 'O', 'Kristen', '085850692024', '081240678467', 'jln.kenanga.no.50,tual,maluku tenggara,maluku', 'sagan CT V 69', 'Sanata Karya Langgur', 'tual', 6, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-13 09:03:45', '2016-06-13 15:07:41'),
(90, '71160027', 22, 'MELKHI PRYADI', 'Laki-laki', 'Tanjung Selor', '1999-02-13', 'O', 'Kristen', '081351925372', '081346493836', 'Jl. Sabanar Lama, Tanjung Selor, Bulungan, Kalimantan Utara', 'Jl. Solo km 11, Gg. Cendana RT/RW 01/01', 'SMA Negeri 1 Tanjung Selor', 'Tanjung Selor', 1, 'S', 'Tidak Ada Penyakit', 'debu', 'Radang Paru - Paru ', '-', 'sesak dan batuk', 'Sesak', '-', 'Ya', '2016-06-13 09:41:30', '2016-06-13 15:08:32'),
(91, '71160038', 24, 'JULIA CHRISTIN SEMBOR', 'Perempuan', 'WAMENA', '1998-07-22', 'O', 'Kristen', '6281218351822', '6281248215533', 'JALAN BHAYANGKARA WAMENA', 'JALAN JAWA SELOKAN MATARAM', 'SMA YPPK St. THOMAS WAMENA', 'WAMENA', 1, 'S', 'Tidak Ada Penyakit', 'DINGIN', 'BATUK DAN FLU', '-', 'HIDUNG SAKIT', 'BATUK DAN PILEK', 'MINYAK KAYU PUTIH', 'Ya', '2016-06-13 10:06:18', '2016-06-13 15:10:07'),
(92, '31160007', 1, 'YEMIMA VIKTORY UTOMO', 'Perempuan', 'BENGKULU', '1999-01-31', 'O', 'Kristen', '6281337863963', '6281339413613', 'JALAN CAK MALADA NOMOR 28, FONTEIN KUPANG.', 'KLITREN LOR', 'SMA NEGERI 3 KUPANG', 'KUPANG', 3, 'M', 'Tidak Ada Penyakit', 'DEBU', 'ASMA', '-', 'BERSIN', 'SESAK', 'MADU DAN JERUK NIPIS', 'Ya', '2016-06-13 10:11:49', '2016-06-13 15:06:07'),
(93, '72160016', 5, 'SION CHRISTOFER PATRIC MHANUAI', 'Laki-laki', 'JAYAPURA', '1997-07-01', 'A', 'Kristen', '081212126538', '0967522995', 'JL.TENGGIRI NO.18 ARGAPURA BAWAH JAYAPURA', 'BINTARAN KIDUL', 'SMK YPK PAULUS DOK V', 'JAYAPURA', 2, 'L', 'Tidak Ada Penyakit', 'DINGIN', 'MALARIA', '-', 'MASUK ANGIN DAN KEMBUNG', 'DEMAM PANAS TINGGI', 'OBAT MALARIA', 'Tidak', '2016-06-13 10:21:50', '2016-06-13 15:10:37'),
(94, '12160023', 6, 'SILVIA KARTIKA KRISTIANI', 'Perempuan', 'METRO', '1998-06-22', 'B', 'Kristen', '085279319222', '081369096888', 'BUMI ASRI RT 021/009 BUMIHARJO BATANGHARI, LAMPUNG TIMUR', 'TERBAN', 'SMA KRISTEN 1 METRO', 'LAMPUNG', 7, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'FLU', '-', 'RINOSH', 'Ya', '2016-06-13 10:32:42', '2016-06-13 15:04:38'),
(95, '11160001', 4, 'YUDI DJAJA', 'Laki-laki', 'YOGYAKARTA', '1998-05-08', 'A', 'Katholik', '089627878458', '08122958973', 'JL.LANGENSARI NO 31', 'JL LANGENSARI NO 31', 'SMA BOPKRI SATU', 'YOGYAKARTA', 8, 'XXXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-13 10:47:31', '2016-06-13 15:03:37'),
(96, '62160003', 19, 'LEONARDO MARCELLINO', 'Laki-laki', 'JAKARTA', '1998-08-03', 'AB', 'Katholik', '087788022353', '08158251789', 'APARTEMENT CITY RESORT TOWER MARIGOLD LANTAI 5 NOMOR 15. CENGKARENG JAKARTA BARAT', '-', 'SMA CINTA KASIH TZU CHI', 'JAKARTA', 5, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-13 10:55:38', '2016-06-13 15:07:56'),
(97, '71160039', 16, 'FRESNI JULVIANA', 'Perempuan', 'TABALONG', '1998-07-02', 'AB', 'Kristen', '081348255557', '081348472094', 'DESA PASAR PANAS', '-', 'SMA FRATER DON BOSCO', 'BANJARMASIN', 1, 'S', 'Tidak Ada Penyakit', '-', '-', 'SEAFOOD(UDANG,CUMI-CUMI,KEPITING)', 'GATAL DITENGGOROKAN', '-', 'MINUM AIR PUTIH UNTUK MEREDEKAN GATAL DITENGGOROKAN', 'Tidak', '2016-06-13 11:18:49', '2016-06-13 15:09:51'),
(98, '12160008', 32, 'NI PUTU GITA RAHMANIATI', 'Perempuan', 'DENPASAR', '1998-04-29', 'A', 'Kristen', '085738415022', '082146178420', 'JL JEMPINIS NO 10 BR UMAKEPUH BUDUK MENGWI BADUNG BALI', 'IROMEJAN GK 3 NO 801 GG PAKEL KLITREN YOGYAKARTA', 'SMAK HARAPAN', 'DENPASAR', 7, 'S', 'Tidak Ada Penyakit', '-', '-', 'SEMUA JENIS SEAFOOD', 'MERAH GATAL', '-', 'OBAT MINUM ATAU SALEP', 'Ya', '2016-06-13 11:42:47', '2016-06-13 15:04:58'),
(99, '12160024', 11, 'ANDRIAN JANUARIUS FAIDARI DUWIRI', 'Laki-laki', 'ABEPURA', '1998-01-07', 'B', 'Kristen', '082399498440', '085254477148', 'BTN ATAS KAMKEY ABEPURA', 'SETURAN 1', 'SMA NEGERI 1JAYAPURA', 'JAYAPURA', 7, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-13 11:52:53', '2016-06-13 15:05:25'),
(100, '11160026', 18, ' MATHIAS ARNOLD LORWENS', 'Laki-laki', 'MERAUKE', '1998-04-25', 'B', 'Kristen', '6285244239325', '6285244285955', 'JL.SERINGGU MERAUKE- PAPUA', 'PURWOMARTANI PERUM PERMATA NO.C8', 'SMA KATOLIK YAN SMIT', 'ASMAT', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-13 11:56:02', '2016-06-13 15:04:01'),
(101, '41160026', 11, 'PUTU VEBY ANGELIKA', 'Perempuan', 'DENPASAR', '1998-05-19', 'A', 'Kristen', '082247510347', '08164746353', 'JL. ARJUNA NO. 3, BR. UMACANDI, BUDUK, MENGWI, BADUNG', '-', 'SMA N 1 KUTA UTARA', 'BALI', 10, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-13 12:14:01', '2016-06-13 15:06:30'),
(102, '71160040', 28, 'RENDY MECKY MORES LUTUR', 'Laki-laki', 'AMBON', '1998-10-12', 'O', 'Kristen', '081240015403', '085343018022', 'AMBON, RUMAHTIGA', 'TERBAN', 'SMA NEGERI 3 AMBON', 'AMBON', 1, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-13 12:15:43', '2016-06-13 15:08:41'),
(103, '41160023', 23, 'RAYMOND DWI PRASETYA', 'Laki-laki', 'GUNUNG KIDUL', '1999-02-07', 'B', 'Kristen', '081904789209', '081328746174', 'WURYANTORO RT 2/3, WURYANTORO, WONOGIRI, JAWA TENGAH', '-', 'SMA N 2 WONOGIRI', 'WONOGIRI', 10, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-13 12:19:47', '2016-06-13 15:07:07'),
(104, '41160028', 19, 'DESAK NYOMAN FRILLA SASTRA CAHYANI', 'Perempuan', 'BALI', '1998-04-17', 'O', 'Kristen', '085935488017', '087862402852', 'BANJAR MUNDUK GAWANG, DESA BELATUNGAN, KEC. PUPUAN, KAB. TABANAN, BALI', 'GODEAN', 'SMA NEGERI 1 TABANAN', 'BALI', 10, 'S', 'Tidak Ada Penyakit', '- ', '-', '- ', '-', '-', '-', 'Ya', '2016-06-13 12:22:02', '2016-06-13 15:06:37'),
(105, '71150042', 31, 'DAVID IMMANUEL WIDAGDO', 'Laki-laki', 'KUDUS', '1996-02-03', 'B', 'Kristen', '081918710777', '08562675100', 'PEMUDA 46 KUDUS', 'TAMBAKBAYAN 16/17C BABARSARI', 'SMA KANISIUS', 'KUDUS', 1, 'empty', 'Tidak Ada Penyakit', '-', 'OPERASI DI LEHER', 'MEMEGANG BESI BERKARAT', '-', '-', '-', 'Ya', '2016-06-13 12:34:17', '2016-06-13 15:08:49'),
(106, '71160041', 27, 'KEVIN HENDRY BATUWAEL', 'Laki-laki', 'AMBON', '1998-06-07', 'O', 'Kristen', '082399352512', '082399489010', 'JL.RABIADJALA', 'KLITREN', 'SMA NEGERI 1 PULAU-PULAU ARU', 'DOBO', 1, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-13 12:36:51', '2016-06-13 15:09:03'),
(107, '12160026', 28, 'DEA MEILINA BR KARO SEKALI', 'Perempuan', 'KABANJAHE', '1998-05-19', 'O', 'Kristen', '085277065467', '085261272942', 'DESA SEBERAYA KC TIGAPANAH ', 'JL. JANTI KANOMAN GG. KENANGA II NO. 362 BANGUNTAPAN BANTUL YOGYAKARTA', 'SMA NEGERI 1 BERASTAGI', 'BERASTAGI', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-13 13:02:09', '2016-06-13 15:05:11'),
(108, '12160025', 17, 'ROBERT SUMANTO', 'Laki-laki', 'KEBUMEN', '1997-08-20', 'AB', 'Katholik', '08985590922', '081327510001', 'JALAN YOS SUDARSO BARAT 25 GOMBONG', 'JALAN MENTRI SUPENO 75,YOGYAKARTA', 'SMA PIUS GOMBONG', 'GOMBONG', 7, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-13 13:11:10', '2016-06-13 15:05:38'),
(109, '71160042', 20, 'CHRISTIANTI ANGELIN MAARENDE', 'Perempuan', 'TONDANO', '1998-11-11', '', 'Kristen', '085242869607', '085341611310', 'KEL.MELONGUANE TIMUR KEC.MELONGUANE KAB.KEPL.TALAUD PROVINSI SULAWESI UTARA', 'KLITREN LOR GK 3/236 RT 11 RW 03 KECAMATAN GONDOKUSUMAN, YOGYAKARTA', 'SMA KRISTEN 2 BINSUS TOMOHON', 'TOMOHON', 1, 'S', 'Tidak Ada Penyakit', '-', 'MAAG, PERNAH PATAH TULANG DI BAHU SEBELAH KANAN', '-', '-', 'NYERI DI AREA DIAFRAGMA, JIKA MEMBAWA BEBAN BERAT SERING MERASA NYERI', 'MILANTA', 'Ya', '2016-06-13 14:02:12', '2016-06-13 15:09:38'),
(110, '41160017', 22, 'AMELITA ROSALINA', 'Perempuan', 'TENGGARONG', '1998-10-20', 'O', 'Katholik', '6282155742561', '6281253830688', 'JALAN GAJAH MADA, BARONG TONGKOK, KUTAI BARAT ,KALIMANTAN TIMUR', 'DR.WAHIDIN 60', 'SMA STELLA DUCE 2', 'YOGYAKARTA', 10, 'M', 'Tidak Ada Penyakit', '-', 'PERNAH OPERASI TELAPAK TANGAN KANAN', 'UDANG DAN KEPITING', 'SESAK NAFAS', 'KALAU ANGKAT BEBAN BERAT NYERI', '-', 'Ya', '2016-06-13 14:19:37', '2016-06-13 15:06:46'),
(111, '72160017', 9, 'SUPIYANTO', 'Laki-laki', 'SEI. TEPANG', '1996-06-07', '', 'Kristen', '082254521067', '081258887539', 'DUSUN JAYA KARYA RT/RW 005/005 MANGGALLA ,PINOH SELATAN, KAB. MELAWI, KALIMANTAN BARAT', 'PENGOK JL. MUTIARA KM 5', 'SMA KRISTEN EKKLESIA ', 'NANGA PINOH, MELAWI', 2, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-14 09:12:13', '2016-06-14 15:09:52'),
(112, '11160027', 8, 'BAMBANG', 'Laki-laki', 'akamayau', '1994-08-10', '', 'Kristen', '082254660478', '081349620219', 'DUSUN AKMAYAU, RT/RW: 001/002, KEL/DESA/: SUNGAI PINANG: KECAMATAN : PINOH UTARA, KABUPATEN MELAWI, KALIMANTAN BARAT.', 'PENGOK JLN. MUTIARA, KM 5(PEMILIK KOS PAK KUSNO)\r\n', 'SMA KRISTEN EKKLESIA', 'NANGA PINOH', 8, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-14 09:15:57', '2016-06-14 15:03:40'),
(113, '61160013', 24, 'MUHAMMAD YUSUF', 'Laki-laki', 'LAPE', '1998-05-15', 'B', 'Islam', '6285218049131', '6287863722286', 'JL.LINTAS SUMBAWA-BIMA', '-', 'SMA NEGERI 1 LAPE', 'LAPE', 6, 'M', 'Tidak Ada Penyakit', '-', '-', 'UDANG', 'GATEL-GATEL', '-', 'OBAT ALERGI', 'Tidak', '2016-06-14 09:29:42', '2016-06-14 15:07:14'),
(114, '61160032', 17, 'JALU CECAR PRASASTHA CATUR PAMUNGKAS', 'Laki-laki', 'YOGYAKARTA', '1998-01-26', 'B', 'Kristen', '082136558261', '081328374260', 'JL.KALIURANG KM 8,2 PRUJAKAN NGAGLIK SLEMAN', 'JL.KALIURANG KM 8,2 PRUJAKAN NGAGLIK SLEMAN', 'SMA BOPKRI SATU ', 'YOGYAKARTA', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-14 10:24:49', '2016-06-14 15:07:40'),
(115, '12160007', 2, 'MELODY MITZI', 'Perempuan', 'YOGYAKARTA', '1997-11-19', 'B', 'Kristen', '085868651172', '087739371173', 'SEMAKI GEDE UH I / 89A \r\nRT17/RW6\r\nYOGYAKARTA', 'SEMAKI GEDE UH I / 89A \r\nRT17/RW6\r\nYOGYAKARTA', 'SMA BOPKRI 1 ', 'YOGYAKARTA', 7, 'S', 'Tidak Ada Penyakit', 'Debu \r\nDingin', '-', 'Makanan pedas', 'Sesak nafas', '-', 'Nasacort', 'Ya', '2016-06-14 10:37:19', '2016-06-14 15:05:14'),
(116, '72160008', 18, 'NETRATNA KRISNARA BUDI', 'Perempuan', 'YOGYAKARTA', '1998-10-16', 'O', 'Kristen', '082243853542', '08122964998', 'JALAN ADAS NO.233 SOROWAJAN BANGUNTAPAN BANTUL ', 'JALAN ADAS NO.233 SORAWAJAN BANGUNTAPAN BANTUL ', 'SMA BOPKRI SATU ', 'YOGYAKARTA ', 2, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-14 10:38:28', '2016-06-14 15:10:12'),
(117, '11160007', 3, 'DEVIANA NATAL RIA', 'Perempuan', 'YOGYAKARTA', '1997-12-31', 'O', 'Kristen', '081326650932', '087838171742', 'Jl. Badran Jt 1/997', 'Jl. Badran Jt 1/997', 'SMA BOPKRI SATU', 'YOGYAKARTA', 8, 'M', 'Tidak Ada Penyakit', '-', 'MAGH. VERTIGO', '-', '-', 'MAGH = SAKIT PERUT\r\nVERTIGO = SAKIT KEPALA (KALAU KECAPEKAN)', 'MERTIGO', 'Tidak', '2016-06-14 10:45:50', '2016-06-14 15:04:05'),
(118, '72160007', 10, 'ADITYA YOGA NUGRAHA', 'Laki-laki', 'SLEMAN', '1998-01-17', 'AB', 'Kristen', '082137848378', '081328791051', 'JLN.KALIURANG KM 12,5 PERUM DUTA WACANA RT 06 RW 09 KARANG BOLONG WONOSALAM NGAGLIK SLEMAN', 'JLN.KALIURANG KM 12,5 PERUM DUTA WACANA RT 06 RW 09 KARANG BOLONG WONOSALAM NGAGLIK SLEMAN', 'SMK MARSUDI LUHUR 1 YOGYAKARTA', 'YOGYAKARTA', 2, 'XL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-14 11:04:49', '2016-06-14 15:10:22'),
(119, '31160017', 4, 'NOVIANTI BARLIN ', 'Perempuan', 'TANA TORAJA', '1999-11-04', '', 'Kristen', '082345240045', '085298789257', 'RANTEPAO, TORAJA UTARA', 'JLN.GAMBIRAN GANG MEMANDELA ', 'SMA KATOLIK RANTEPAO', 'RANTEPAO', 3, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-14 11:14:53', '2016-06-14 15:06:03'),
(120, '11160029', 12, 'TUTUT HERIYANTI', 'Perempuan', 'JAWAI LAUT', '1998-05-31', '', 'Islam', '085604699845', '082153865593', 'DESA JAWAI LAUT RT 01 RW 02 KALIMANTAN BARAT', '-', 'SMA N 1 JAWAI', 'SAMBAS ', 8, 'S', 'Tidak Ada Penyakit', 'DEBU\r\nDINGIN ', 'MAAG', 'MAKANAN LAUT', ',MAKANAN LAUT : MERAH - MERAH / GATAL - GATAL\r\nDEBU : SESAK NAFAS\r\nDINGIN : SESAK NAFAS', 'SAKIT PERUT', 'MAAG : PROMAGH', 'Tidak', '2016-06-14 11:25:15', '2016-06-14 15:04:30'),
(121, '11160028', 14, 'RESI ROSANTI', 'Perempuan', 'BAKAU', '1997-04-19', 'O', 'Islam', '082370520428', '085252459023', 'DESA BAKAU KECAMATAN JAWAI KABUPATEN SAMBAS , KALBAR', '-', 'SMA N 1 JAWAI', 'KALBAR', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', 'MAGH, PUSING ', '-', 'BERSIN BERSIN', 'MUAL, NYERI PERUT ,', 'PROMAGH, BODREX', 'Ya', '2016-06-14 11:26:24', '2016-06-14 15:04:44'),
(122, '62160004', 23, 'AMELIA ANGELIKA', 'Perempuan', 'BANDUNG', '1998-05-15', 'B', 'Kristen', '081225484099', '081215773633', 'JL. BESKALAN NO. 35, YOGYAKARTA', 'JL. BESKALAN NO. 35, YOGYAKARTA', 'SMA BOPKRI 1 ', 'YOGYAKARTA', 5, 'M', 'Tidak Ada Penyakit', '-', 'MAAG ', 'PEDAS', 'MAAG KAMBUH, NYERI PERUT', 'NYERI PERUT', 'NEXIUM', 'Ya', '2016-06-14 11:55:27', '2016-06-14 15:09:04'),
(123, '12160027', 7, 'SERFASIUS GHUNU DOWA', 'Laki-laki', 'BONDOKAWANGO', '1995-04-15', '', 'Katholik', '082189553910', '085205628274', ' JLN KOTA TAMBOLAK SUMBA BARAT DAYA', ' JLN KLEDOKAN LEMPUYANGAN', 'SMAK ST THOMAS AUINAS', 'SBD KOTA TAMBOLAK', 7, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-14 11:58:36', '2016-06-14 15:05:31'),
(124, '01160009', 33, 'ABDI SABDA WINEDAR', 'Laki-laki', 'BEKASI', '1998-06-25', 'O', 'Kristen', '089637529748', '08159448502', 'JL. MUTIARA RAYA BLOK A NO 85 BMP BEKASI', 'ASRAMA UKDW', 'SMAK PENABUR HARAPAN INDAH ', 'BEKASI', 4, 'XXXL', 'Tidak Ada Penyakit', '-', '-', 'UDANG ', 'GATAL-GATAL ,MERAH-MERAH ', '-', '-', 'Ya', '2016-06-14 12:10:53', '2016-06-14 15:02:21'),
(125, '31160018', 13, 'VIONA CH SALAKORY', 'Perempuan', 'AMBON', '1998-12-08', 'O', 'Kristen', '085336693649', '081240256001', 'JL WOLTER MONGINSIDI LATTA AMBON', '-', 'SMA NEGERI 4  AMBON', 'AMBON', 3, 'S', 'Tidak Ada Penyakit', 'DEBU DAN DINGIN', 'MAGH', '-', 'SESAK, BERSIN-BERSIN', 'NYERI PERUT', 'OMEPRAZOLE', 'Tidak', '2016-06-14 12:53:15', '2016-06-14 15:06:22'),
(126, '01160010', 16, 'XAVIER KHARIS', 'Laki-laki', 'JAKARTA', '1998-03-21', 'B', 'Kristen', '6281280719370', '628129533174', 'JLN. BELIMBING II NO. 19 DEPOK, JAWA BARAT 16431', 'ASRAMA UKDW', 'SMA PSKD VII DEPOK', 'DEPOK', 4, 'XXXXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-15 08:49:11', '2016-06-15 15:02:47'),
(127, '11160024', 21, 'ARDYA IMANNUELA KHARISMA PRIMA DHENINTA', 'Perempuan', 'GUNUNG KIDUL', '1998-06-17', 'O', 'Katholik', '085787247394', '085652320575', 'JL.GAJAH MADA GG.FAMILY\r\nKETAPANG, KALBAR', 'JL. AFFANDI', 'SMA PL. SANTO YOHANES KETAPANG', 'KETAPANG', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', 'PERNAH PATAH TULANG BAGIAN PERGELANGAN TANGAN', '-', 'FLU', 'LINU', 'ISTIRAHAT, MASKER', 'Ya', '2016-06-15 09:15:18', '2016-06-15 15:03:41'),
(128, '12160005', 29, 'DIANTY MANUPUTTY', 'Perempuan', 'WAMENA', '1999-01-29', 'O', 'Kristen', '6282399729605', '6285244369256', 'JL. PANJAITAN NO 18 E WAMENA,PAPUA', 'JL KEMIRI  NO.201 D', 'SMA STELLA DUCE 2', 'YOGYAKARTA', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-15 09:51:58', '2016-06-15 15:05:13'),
(129, '11160030', 27, 'JUNITA MARIA DE JESUS', 'Perempuan', 'LORI', '1994-06-20', 'A', 'Katholik', '081236076146', '081327565864', 'PANTAI KELAPA,DILI TIMOR-LESTE', 'JL.RAJAWALI GANG PARKIT NO 7 DEMANGAN BARU', '10 DE DEZEMBRO COMORO DILI', 'DILI', 8, 'M', 'Tidak Ada Penyakit', 'DEBU\r\nHUJAN', '-', '-', 'DEBU : FLU\r\nHUJAN : SAKIT KEPALA', '-', '-', 'Tidak', '2016-06-15 10:18:55', '2016-06-15 15:03:50'),
(130, '61160034', 26, 'ASTRY PUTRI DAYANDA ANABOKAY', 'Perempuan', 'MARDEKA', '1998-12-03', '', 'Kristen', '085333013935', '085239128702', 'JL FRANS DA ROMES MAULAFA-KUPANG-NTT', '-', 'SMA NEGERI 1 KUPANG', 'KOTA KUPANG', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-15 10:28:24', '2016-06-15 15:07:40'),
(131, '12160028', 30, 'ELSA INDRIANI SITOMPUL', 'Perempuan', 'MUARA TEWEH', '1998-03-25', 'AB', 'Kristen', '6281223150338', '6285249692611', 'JL. BHAYANGKARA GG. KINIBALU, BARITO UTARA, KAL-TENG', 'JL. TUNJUNG BARU, KOMPLEK BACIRO', 'SMAN-1 MUARA TEWEH', 'MUARA TEWEH', 7, 'S', 'Tidak Ada Penyakit', 'DEBU DAN UDARA DINGIN', 'MAGH', 'ALERGI UDANG DAN KEPITING', 'UDANG DAN KEPITING; TENGGOROKAN GATAL\r\nDEBU DAN UDARA DINGIN; FLU', 'MUAL', 'JANGAN SAMPAI TELAT MAKAN', 'Ya', '2016-06-15 10:45:58', '2016-06-15 15:05:24');
INSERT INTO `pesertas` (`id`, `nim`, `idkelompok`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `golongan_darah`, `agama`, `nomor_telpon`, `nomor_orangtua`, `alamat_asal`, `alamat_jogja`, `asal_sekolah`, `kota_asal_sekolah`, `idprodi`, `ukuran_almamater`, `jenis_penyakit`, `alergi`, `penyakit_diderita`, `alergi_makanan`, `ralergi`, `rpenyakit`, `obat`, `tonti`, `created`, `modified`) VALUES
(132, '31160019', 31, 'ABNER AMADEUZ WISAKSONO', 'Laki-laki', 'BANDUNG', '1998-11-26', 'O', 'Kristen', '087821555211', '0816626372', 'JALAN MEGASARI NO.7 PERUM MEGARAYA SUKARAJA II', '-', 'SMAK 2 BPK PENABUR', 'BANDUNG', 3, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-15 10:48:06', '2016-06-15 15:06:33'),
(133, '41160021', 33, 'INDRIANI NUR AZIZAH', 'Perempuan', 'TEGAL', '1996-06-10', 'AB', 'Islam', '081243323804', '0811591967', 'KLITREN LOR GK III/280 RT 014 RW 004', 'KLITREN LOR GK III/280 RT 014 RW 004', 'SMA ANGKASA', 'SLEMAN', 10, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-15 11:00:34', '2016-06-15 15:07:01'),
(134, '31160020', 26, 'ANILA AGUSTINA HEI', 'Perempuan', 'NABIRE', '1996-04-10', '', 'Kristen', '082242202197', '081240207524', 'NABIRE KAMPUNG SAMABUSA', 'PANTAI WRENA', 'SMK KESEHATAN NABIRE ', 'NABIRE', 3, 'M', 'Tidak Ada Penyakit', 'UDARA (PENYESUAIAN CUACA/ADAPTASI TEMPAT TINGGAL)', 'MAAG', '-', 'GATAL', 'PERIH, LAMBUNG SAKIT', 'MYLANTA', 'Ya', '2016-06-15 11:55:41', '2016-06-15 15:05:53'),
(135, '71160034', 1, 'AGNES MARTEN NAMANGDJABAR', 'Laki-laki', 'ALOR', '1999-08-09', 'O', 'Kristen', '082147337740', '082339607038', 'SEIENG RT 003 RW 002 DES. AIMOLI KEC. ALOR BARAT LAUT KAB. ALOR', 'JLN MUJA MUJU UH 2/1087 RT32 RW 10 YOGYAKARTA ', 'SMA KRISTEN 1 KALABAHI', 'KALABAHI', 1, 'M', 'Tidak Ada Penyakit', 'CUACA DINGIN', '-', '-', 'MENGGIGIL', '-', '-', 'Ya', '2016-06-15 12:04:44', '2016-06-15 15:08:07'),
(136, '41160037', 34, 'SINTA PUTRI NIRMALA', 'Perempuan', 'KLATEN', '1998-03-12', 'O', 'Kristen', '081233599652', '081548589706', 'BATURAN RT.11/RW.04, BULUSAN, KARANGDOWO, KLATEN, JAWA TENGAH', 'GANG PERKUTUT, DEMANGAN BARU', 'SMA N 1 CAWAS', 'KLATEN', 10, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-15 12:31:57', '2016-06-15 15:07:07'),
(137, '01160011', 25, 'RYAN CHANDRA RIUPASSA', 'Laki-laki', 'PANGKALAN BUN', '1998-08-30', 'O', 'Kristen', '087770899685', '081318184251', 'VILLA BOGOR INDAH BLOK CC 15 NO 12', 'ASRAMA TEOLOGI', 'SMA KESATUAN BOGOR', 'BOGOR', 4, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-15 12:57:26', '2016-06-15 15:02:57'),
(138, '11160031', 15, 'KOMANG BUDI ARNAWAN', 'Laki-laki', 'DENPASAR', '1998-08-08', 'O', 'Hindu', '628973865513', '6281246533906', 'JLN GUNUNG KARANG GANG 4 NO 2', '-', 'SMA NEGERI 5 DENPASAR', 'DENPASAR', 8, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-15 13:02:17', '2016-06-15 15:04:34'),
(139, '61160035', 12, 'BEREKHYA YONARISCHA', 'Perempuan', 'KAB. SEMARANG', '1998-08-15', 'O', 'Kristen', '087700153917', '6281390828695', 'PUNDUNG PUTIH NO. 11, RT01/RW03, GEDANGANAK, KEC. UNGARAN TIMUR, KAB SEMARANG', '-', 'SMA KRISTEN SATYA WACANA', 'SALATIGA', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-16 09:06:44', '2016-06-16 15:03:16'),
(140, '01160012', 17, 'GRASEILA KRISTIANTIA', 'Perempuan', 'JAKARTA', '1998-02-24', 'O', 'Kristen', '087757060340', '082125517098', 'JALAN ADIL NOMOR 21 RT 04 RW 06\r\nLUBANG BUAYA, JAKARTA TIMUR 13810', 'ASRAMA UKDW', 'SMAN 67 JAKARTA', 'JAKARTA TIMUR', 4, 'S', 'Tidak Ada Penyakit', '-', 'THYPUS', '-', '-', '-', '-', 'Ya', '2016-06-16 09:23:08', '2016-06-16 15:00:29'),
(141, '72160018', 4, 'ALFREDO ANUGRAH', 'Laki-laki', 'BANPRES', '1998-10-29', 'O', 'Kristen', '6282191381691', '6281391186505', 'PALU ( SULAWESI TENGAH )', 'JLN. NYIADISORO KOTA GEDE 2', 'SMK KOPERASI', 'YOGYAKARTA', 2, 'M', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'PUSING', 'PARASETAMOL', 'Ya', '2016-06-16 10:44:40', '2016-06-16 15:05:04'),
(142, '72160019', 6, 'RAMANA CHRISTOFER PASKALIS', 'Laki-laki', 'DAK JAYA', '1998-04-09', 'B', 'Katholik', '628976993680', '628125777343', 'DUSUN SANDUNG RT 001 RW 001 KECAMATAN BINJAI HULU PROVINSI KALIMANTAN BARAT', 'PUGERAN ', 'SMA SINAR KASIH ', 'SINTANG', 2, 'M', 'Tidak Ada Penyakit', '-', 'MALARIA MAAG', '-', '-', 'SAKIT KEPALA', '-', 'Tidak', '2016-06-16 11:22:22', '2016-06-16 15:05:14'),
(143, '41160018', 2, 'YEREMIA WICAKSONO PUTRO', 'Laki-laki', 'YOGYAKARTA', '1998-04-08', 'B', 'Kristen', '6285725721709', '6281578718429', 'IROMEJAN GK3/604B YOGYAKARTA', 'IROMEJAN GK3/604B YOGYAKARTA', 'SMA N 9 ', 'YOGYAKARTA', 10, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-16 11:41:30', '2016-06-16 15:02:26'),
(144, '41160041', 31, 'VIRASARI NIKEN DYAHAYU KUMARANING GUSTI', 'Perempuan', 'YOGYAKARTA', '1998-06-10', 'B', 'Kristen', '6285868278928', '62816683450', 'COKRODININGRATAN JT II/146 RT 015/RW 004 COKRODININGRATAN, JETIS, YOGYAKARTA, DIY', 'PERUM GRIYA KETAWANG PERMAI P-8 RT 03/RW 38 AMBARKETAWANG, GAMPING, SLEMAN, DIY', 'SMA NEGERI 11 YOGYAKARTA', 'YOGYAKARTA', 10, 'S', 'Tidak Ada Penyakit', '-', 'DBD, SESAK NAPAS', '-', '-', 'PUSING, SESAK', 'TEH, OKSIGEN', 'Ya', '2016-06-16 11:45:48', '2016-06-16 15:02:40'),
(145, '72160020', 8, 'GARNIDA ANGGIT CRISTIAN FIRERI', 'Laki-laki', 'CILACAP', '1998-07-11', 'B', 'Katholik', '6289692103158', '6281326878882', 'PERUMAHAN TAMAN PATRA INDAH BLOK C1 NO.9 CILACAP', 'JALAN RAJAWALI GANG MAYANG VI/07 PRINGGOLAYAN BANTUL YOGYAKARTA 01/44', 'SMA YOS SUDARSO CILACAP', 'CILACAP', 2, 'S', 'Tidak Ada Penyakit', '-', 'DEMAM BERDARAH', '-', '-', 'PUSING', 'JAMBU BIJI', 'Tidak', '2016-06-16 11:59:41', '2016-06-16 15:05:24'),
(146, '11094853', 11, 'LEO HARYANTO SIMANULLANG', 'Laki-laki', 'PEMATANGSIANTAR', '1990-08-01', 'AB', 'Kristen', '085290997490', '081396928680', 'JALAN MAHONI RAYA NO 58. PEMATANG SIANTAR', 'JALAN WIRAJAYA NO 157. GEJAYAN, CONDONGCATUR. SLEMAN', 'SMA NEGERI 3 PEMATANGSIANTAR', 'PEMATANGSIANTAR', 8, 'empty', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-16 12:05:08', '2016-06-16 15:01:25'),
(147, '81160001', 9, 'ANTONIUS EDWIN BUDI KHARISMA', 'Laki-laki', 'YOGYAKARTA', '1997-04-15', 'A', 'Katholik', '628888416161', '628122963434', 'JL Ayani 165 Purworejo', '-', 'SMA BRUDERAN PURWOREJO', 'PURWOREJO', 12, 'M', 'Tidak Ada Penyakit', 'Debu', 'Asma', '-', 'Pilek', 'Batuk dan Sesak', 'Antibiotik,Neosep.', 'Tidak', '2016-06-16 12:32:32', '2016-06-16 15:05:43'),
(148, '71160013', 3, 'GIOVANO VALLERIAN PALEVA', 'Laki-laki', 'SALATIGA', '1998-03-06', 'O', 'Kristen', '085727239795', '082134955445', 'TAMAN PAHLAWAN 36 SALATIGA', 'POGUNG KIDUL SIA XVI-VI/2 JL. KALIURANG KM 4.5 YOGYAKARTA', 'SMA KRISTEN SATYA WACANA', 'SALATIGA', 1, 'XXXXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-16 13:57:53', '2016-06-16 15:04:34'),
(149, '61160037', 18, 'HENGKY KURNIAWAN', 'Laki-laki', 'Sintang', '1988-10-13', 'O', 'Kristen', '6282154055116', '628155775982', 'Sintang, Kalimantan barat', '-', 'SMA Panca Setya', 'Sintang', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-17 08:38:43', '2016-06-17 15:04:34'),
(150, '61160036', 21, 'YONGKY HERMAWAN', 'Laki-laki', 'SINTANG', '1998-10-13', 'O', 'Kristen', '082154801199', '082155775982', 'SINTANG, KALIMANTAN BARAT', '-', 'SMA PANCA SETYA ', 'SIANTANG, KALIMANTAN BARAT', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 08:41:01', '2016-06-17 15:04:52'),
(151, '62160005', 15, 'CINTHIA SETIAWATI', 'Perempuan', 'SURAKARTA', '1995-04-10', 'O', 'Kristen', '6285714834027', '62816673900', 'jln.kh saman hudi no115a sondakan laweyan solo', 'jln. kaswari no 7 demangan baru', 'SMA WIDYA WACANA SURAKARTA', 'SURAKARTA', 5, 'M', 'Tidak Ada Penyakit', '-', 'vertigo', '-', '-', 'pusing,mual , muntah', 'mertigo', 'Ya', '2016-06-17 08:53:41', '2016-06-17 15:06:02'),
(152, '41160022', 10, 'AHMAD FAUZI AZHARI', 'Laki-laki', 'MOJOKERTO', '1996-12-04', 'O', 'Islam', '6282220300024', '6281340019666', 'BLOK Q-6 LANUD ADISUCIPTO, BANGUNTAPAN, BANGUNTAPAN, BANTUL D.I.YOGYAKARTA 55198', 'BLOK Q-6 LANUD ADISUCIPTO, BANGUNTAPAN, BANGUNTAPAN, BANTUL D.I.YOGYAKARTA 55198', 'SMAN 1 BANGUNTAPAN', 'BANTUL YOGYAKARTA', 10, 'M', 'Tidak Ada Penyakit', '-', '-', 'UDANG', 'GATAL DI TENGGOROKAN', '-', '-', 'Ya', '2016-06-17 09:31:59', '2016-06-17 15:03:37'),
(153, '61160033', 23, 'FRANCISCO ALVES', 'Laki-laki', 'ASSALAINO', '1996-01-24', 'B', 'Katholik', '6281327729010', '+67077390250', 'ASSALAINO, LOSPALOS, TIMOR-LESTE', 'KLITREN LOR GK 3 NO 383', 'STM LOSPALOS, TIMOR-LESTE', 'TIMOR-LESTE', 6, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 09:46:31', '2016-06-17 15:05:00'),
(154, '72160006', 29, 'YOSEPHOCY MINARSO PUTRA', 'Laki-laki', 'SURAKARTA', '1997-09-08', 'O', 'Kristen', '6281802503137', '6281225877474', 'JL. RONGGOWARSITO 113', '-', 'SMA PANGUDI LUHUR SANTO YOSEF', 'SURAKARTA', 2, 'M', 'Tidak Ada Penyakit', 'BULU', '-', 'UDANG', 'SESAK', '-', 'OXYGEN', 'Ya', '2016-06-17 09:53:30', '2016-06-17 15:07:30'),
(155, '71160045', 25, 'ANGGIE RENATA PUTRI', 'Perempuan', 'JAKARTA', '1998-08-14', 'B', 'Kristen', '6287889063277', '6282137849943', 'JL. RAYA TAJEM, NOMPOREJO, SLEMAN, YOGYAKARTA', 'JL. RAYA TAJEM, NOMPOREJO, SLEMAN, YOGYAKARTA', 'SMA BOPKRI 2 YOGYAKARTA', 'YOGYAKARTA', 1, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 09:55:16', '2016-06-17 15:06:53'),
(156, '61160038', 27, 'KOMANG LINGGA PURNAYUDA', 'Laki-laki', 'PALEMBANG', '1998-03-24', 'B', 'Hindu', '6281285760846', '6282280617765', 'TULUNG HARAPAN PALEMBANG', '-', 'SMAN 10 PALEMBANG', 'PALEMBANG', 6, 'M', 'Tidak Ada Penyakit', '-', 'LEHER TEGANG', 'TERI', 'GATAL', 'PUSING', 'BALSEM', 'Ya', '2016-06-17 10:05:53', '2016-06-17 15:05:10'),
(157, '11160032', 22, 'LUBERTUS HENDRI', 'Laki-laki', 'NA SEBERUANG', '1995-08-28', 'AB', 'Katholik', '6285654587245', '6285252184540', 'DUSUN SEBERUANG BESAR', 'SELOKAN MATARAM', 'SMAN 1 SELIMBAU', 'P0NTIANAK', 8, 'S', 'Tidak Ada Penyakit', '-', 'PATAH TULANG TANGAN', 'UDANG', '-', 'NYERI', '-', 'Tidak', '2016-06-17 10:08:36', '2016-06-17 15:02:12'),
(158, '31160005', 14, 'ANNABLLE INDRYANA PURWANTO', 'Perempuan', 'SEMARANG ', '1998-08-18', 'O', 'Kristen', '6281215267442', '6281329057547', 'PLAMONGAN INDAH D-24', 'JL PETUNG NO 36 PAPRINGAN DEMANGAN BARU', 'SMA KRISTEN KALAM KUDUS SUKOHARJO', 'SOLO', 3, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 10:09:59', '2016-06-17 15:03:07'),
(159, '12160029', 5, 'APRILINDA BENGAN GEON', 'Perempuan', 'Oringbele', '1998-04-08', 'AB', 'Katholik', '082257437268', '081240548262', 'Oringbele, Adonara,flores NTT', 'Klitren lor GK 3 no.378 Rt 17 RW 04', 'SMA SYURADIKARA ENDE ', 'ENDE', 7, 'S', 'Tidak Ada Penyakit', '-', 'SESAK NAPAS', '-', '-', 'SESAK', 'Tidur.', 'Ya', '2016-06-17 10:53:20', '2016-06-17 15:02:36'),
(160, '01160013', 7, 'KRIS NUR CAHYANI', 'Perempuan', 'PURWOREJO', '1998-11-28', 'AB', 'Kristen', '6289665360843', '6285647764103', 'SENEPO TIMUR RT 01 RW 01 KUTOARJO, PURWOREJO', 'ASRAMA UKDW', 'SMA NEGERI 1 PURWOREJO', 'PURWOREJO', 4, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'SAKIT PERUT', 'PROMAAG', 'Tidak', '2016-06-17 11:03:11', '2016-06-17 15:01:00'),
(161, '61160003', 28, 'HADI JAYA PUTRA', 'Laki-laki', 'BELINYU', '1998-08-26', 'O', 'Islam', '081274411434', '081373548234', 'JL. RADEN ABDULLAH NO.235B, OPAS INDAH, TAMAN SARI, PANGKALPINANG, BANGKA BELITUNG 33129', 'JL.MAGELANG KM.12 RT/7 RW/3, WADAS, TRIDADI, SLEMAN, YOGYAKARTA 55511', 'SMK NEGERI 2 PANGKALPINANG', 'PANGKALPINANG', 6, 'M', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'BERSIN', '-', 'MASKER', 'Tidak', '2016-06-17 11:21:19', '2016-06-17 15:05:19'),
(162, '71160011', 34, 'ADRIAN JULIO CANDAYA', 'Laki-laki', 'MAGELANG', '1998-07-25', 'O', 'Katholik', '6282221216441', '6281328095137', 'JALAN TEJO SUNARYO NO 29 PARAKAN', 'IROMEJAN', 'SMA SEDES SAPIENTIAE', 'SEMARANG', 1, 'L', 'Tidak Ada Penyakit', 'DEBU', 'ASMA', '-', 'BERSIN MATA MERAH BERAIR', 'SESAK NAFAS', 'OBAT BAWA SENDIRI', 'Tidak', '2016-06-17 11:31:25', '2016-06-17 15:07:12'),
(163, '11160004', 16, 'STEFIANY JESSICA', 'Perempuan', 'SURAKARTA', '1998-06-11', 'A', 'Kristen', '081227161778', '085102136113', 'JL.KH.AGUS SALIM 51', '-', 'SMA WIDYA WACANA SURAKARTA', 'SOLO', 8, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-17 12:10:34', '2016-06-17 15:01:34'),
(164, '61160039', 13, 'RENATA THALIA', 'Perempuan', 'JAKARTA', '1997-08-18', 'B', 'Kristen', '6281237897232', '6282110101076', 'APT.GADING MEDITERANIA, KELAPA GADING, JAKARTA', 'KOST D\'PARAGON, SETURAN', 'ST. JOHN\'S CATHOLIC COLLEGE', 'DARWIN', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 12:22:36', '2016-06-17 15:04:13'),
(165, '61160041', 30, 'RIVALDO GIOVANNY BATISTA MAURY', 'Laki-laki', 'JAKARTA', '1998-07-13', 'O', 'Kristen', '085244657591', '082199686728', 'JL.BRAWIJAYA NO 39', 'KOS D.PARAGON SETURAN', 'SMA NEGERI 1 MANOKWARI', 'MANOKWARI', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 12:23:33', '2016-06-17 15:05:30'),
(166, '61160040', 32, 'BRAM JUNIOR OCTAVIAN', 'Laki-laki', 'JAKARTA', '1998-10-20', 'A', 'Kristen', '085244851895', '082310331977', 'JL. SMK REREMI PUNCAK', 'JL.SETURAN', 'SMA YPK OIKUMENE', 'MANOKWARI', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-17 12:30:28', '2016-06-17 15:05:38'),
(167, '11160033', 19, 'FABYOLA YOVITA PUTRI', 'Perempuan', 'MAKASSAR', '1998-07-27', 'A', 'Kristen', '081355712494', '08111804830', 'JL CESSNA NO 7 KOMPLEKS AURI', '-', 'SMA DIAN HARAPAN', 'MAKASSAR', 8, 'S', 'Tidak Ada Penyakit', 'DEBU DAN BULU', 'SINUSITIS', '-', 'SESAK NAFAS', 'SUSAH BERNAFAS', 'INHEALER/ ASMA SOHO', 'Tidak', '2016-06-17 12:48:44', '2016-06-17 15:01:48'),
(168, '62160006', 24, 'GABRIELLA NADYA ANGGIA', 'Perempuan', 'SEMARANG', '1997-12-19', 'O', 'Kristen', '0895347239125', '6281233222898', 'JL. TAMBORA 26, MALANG', '-', 'SMA CHARIS NATIONAL ACADEMY', 'MALANG', 5, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-17 13:35:30', '2016-06-17 15:06:13'),
(169, '11160036', 20, 'ANGELUS SELMON SERAN', 'Laki-laki', 'WAIKABUBAK SUMBA BARAT', '1998-05-29', 'O', 'Katholik', '6282340501415', '6281339767466', 'JALAN AHMAD YANI NO.113\r\nKELURAHAN MALITI\r\nWAIKABUBAK-SUMBA BARAT', 'BABARSARI', 'SMA NEGERI 1 WAIKABUBAK', 'WAIKABUBAK', 8, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-17 14:18:09', '2016-06-17 15:02:01'),
(170, '01160015', 14, 'GILBERT CHRISTIAN KRISTAMULYANA', 'Laki-laki', 'JAKARTA', '1992-05-21', 'A', 'Kristen', '+6285921783400', '+62818791958', 'JL KARANG ASRI I C2/26 LEBAK BULUS, JAKARTA 12440', '-', 'SMAK TIRTA MARTA BPK PENABUR', 'JAKARTA', 4, 'XL', 'Tidak Ada Penyakit', '-', 'BRONKITIS', '-', '-', 'SESAK', '-', 'Tidak', '2016-06-20 09:47:08', '2016-06-20 15:03:13'),
(171, '61160009', 19, 'HACINIKO YAHYA', 'Laki-laki', 'YOGYAKARTA', '1997-10-04', 'O', 'Kristen', '+6285289043983', '+6281228127439', 'RATMAKAN GM 1/ 655 Kelurahan Ngupasan Kecamatan Gondomanan Kota Yogyakarta', 'RATMAKAN GM 1/ 655 Kelurahan Ngupasan Kecamatan Gondomanan Kota Yogyakarta', 'SMK N 2 DEPOK', 'YOGYAKARTA', 6, 'XL', 'Tidak Ada Penyakit', 'Debu, Dingin', 'Asma, Maag', '-', 'Pilek, Sesak', 'Sesak', '-', 'Ya', '2016-06-20 10:17:38', '2016-06-20 15:05:14'),
(172, '71160046', 2, 'STELLA DODI SALAKORI ', 'Perempuan', 'JAYAPURA', '1998-03-22', 'A', 'Kristen', '6281247359990', '6281326618224', 'JLN. SOSIRI NO.17 ABEPURA ', 'POJOK BETENG WETAN, JL.PARANGTRITIS  24. ', 'SMA YPPK TARUNA BAKTI JAYAPURA', 'JAYAPURA', 1, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-20 10:39:31', '2016-06-20 15:07:45'),
(173, '01160016', 6, 'PRABANDARI PUSPITANING RAHARDIANI', 'Perempuan', 'SURABAYA', '1998-08-11', '', 'Kristen', '6285784410320', '6285604645611', 'PERUMAHAN MENGANTI PERMAI B4/22-23 GRESIK', 'ASRAMA', 'SMA KRISTEN PETRA 5 SURABAYA', 'SURABAYA', 4, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'MUAL, MUNTAH, KERINGAT DINGIN', 'MYLANTA, PROMAAG', 'Tidak', '2016-06-20 10:47:20', '2016-06-20 15:02:31'),
(174, '11160034', 23, 'MARIA PRUDENTIA', 'Perempuan', 'SANDAI', '1998-07-22', '', 'Katholik', '6282154103306', '6282351540335', 'JL.PRAMUKA SANDAI KANAN\r\nKEC.SANDAI \r\nKAB KETAPANG\r\nPROV KALBAR', 'JL.KABUPATEN SLEMAN', 'SMAN 1 SANDAI', 'SANDAI', 8, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', 'KUNING TELUR', 'MUAL', 'MUAL DAN PUSING', 'MINUM MADU UTK ALERGI\r\nPOLISILANE UTK MAAG', 'Tidak', '2016-06-20 11:24:57', '2016-06-20 15:03:56'),
(175, '61160042', 33, 'HARSENOV MAHARANY', 'Laki-laki', 'RANTEPAO', '1997-11-10', 'O', 'Kristen', '6282293626312', '6285146354988', 'PANGRANTE TIMUR, KABUPATEN TORAJA UTARA', 'POGUNG DALANGAN NO.19A RW50 RT10', 'SMA MATARI\'ALLO RANTEPAO', 'RANTEPAO', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-20 12:12:13', '2016-06-20 15:07:11'),
(176, '12160030', 4, 'FLORENSIA INDASARI DERAN SADA LAMAHODA', 'Perempuan', 'WEETABULA', '1999-01-15', 'B', 'Katholik', '6282237165916', '6281339840995', 'WEETABULA,SBD,NTT', 'PAPRINGAN,GANG ORI 1', 'SMAS ST THOMAS AQUINAS', 'WEETABULA', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-20 12:25:53', '2016-06-20 15:04:33'),
(177, '01160017', 12, 'YOEL KATONA RADITYA', 'Laki-laki', 'Semarang', '1996-10-03', 'O', 'Kristen', '6285225147904', '628122848264', 'Jl. Candi Mutiara Timur III/1288, Semarang', 'Asrama UKDW', 'SMA NEGERI 7 SEMARANG', 'SEMARANG', 4, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-20 14:13:40', '2016-06-20 15:02:59'),
(178, '11160037', 25, 'DERNANTO M SIBURIAN', 'Laki-laki', 'DUMAI', '1997-12-08', 'O', 'Kristen', '082285721408', '081371681695', 'DUMAI, JLAN.MERDEKA BARU', 'JLN. BABARSARI', 'PKBM. DAHARMA PUTRA', 'DUMAI', 8, 'L', 'Tidak Ada Penyakit', '_', '_', '_', '_', '_', '_', 'Ya', '2016-06-21 08:40:57', '2016-06-21 15:01:48'),
(179, '01160014', 8, 'DEA EMILIA SARLOTHA PLAIKOIL', 'Perempuan', 'KANDARA', '1998-08-28', 'B', 'Kristen', '082146478210', '081339409780', 'JLN. PERDAMAIAN WANGGA, WAINGAPU', 'BANYUMENENG, SLEMAN', 'SMA NEGERI 2 WAINGAPU', 'WAINGAPU', 4, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-21 08:47:06', '2016-06-21 15:00:48'),
(180, '11160035', 28, 'RISNAWATI MASSORA', 'Perempuan', 'WAMENA', '1998-08-31', 'B', 'Katholik', '082248459656', '081248033139', 'RANTEPAO TORAJA UTARA', '-', 'SMK SMART RANTEPAO ', 'TORAJA UTARA ', 8, 'L', 'Tidak Ada Penyakit', 'DINGIN ', 'ASMA ', '-', 'SESAK', 'SESAK BATUK ', 'VENTOLIN UNTUK ASMA ', 'Ya', '2016-06-21 11:50:55', '2016-06-21 15:02:11'),
(181, '01160018', 9, 'AVE TRECIA KABASARANG', 'Perempuan', 'BULI', '1998-06-15', 'O', 'Kristen', '+6285242597227', '+6281341515476', 'OBETH KABASARANG, BULI HALMAHERA TIMUR', 'ASRAMA TEOLOGI', 'SMA NEGERI 1 HALTIM', 'BULI', 4, 'S', 'Tidak Ada Penyakit', 'DINGIN', 'INFEKSI USUS', '-', 'SESAK NAFAS', 'SAKIT DI BAGIAN PERUT', 'AMOXILIN', 'Ya', '2016-06-21 12:26:47', '2016-06-21 15:01:02'),
(182, '41160030', 17, 'DEBBY KURNIAWAN CHANDRA SAPUTRA', 'Perempuan', 'KUDUS', '1998-12-31', 'O', 'Kristen', '081326799955', '08157710200', 'JALAN AGIL KUSUMADYA (PERUMAHAN JATI PERMAI 81 KUDUS)', 'KLIOTREN LOR-GANG PONCOWATI NO 398', 'SMA KANISIUS', 'KUDUS', 10, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-21 13:31:02', '2016-06-21 15:03:38'),
(183, '12160031', 1, 'GRACE ELISABET LARUMPAA', 'Perempuan', 'MELONGUANE', '1998-07-16', 'O', 'Kristen', '085340459233', '082347202075', 'MELONGUANE BARAT', 'KLITREN LOR GK 3/236 RT 11 RW 03 KECAMATAN GONDOKUSUMAN,YOGYAKARTA', 'SMA NEGERI 1 MELONGUANE', 'MELONGUANE', 7, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', 'TELUR', 'GATAL-GATAL, SESAK NAFAS, BERSIN', '-', '-', 'Ya', '2016-06-21 13:37:37', '2016-06-21 15:02:36'),
(184, '71160015', 5, 'ANDREAS KURNIAWAN PRAJITNO', 'Laki-laki', 'SLEMAN', '1998-02-16', 'O', 'Kristen', '0811269625', '0811269625', 'KAJOR BARU 14B JLN GODEAN KM 4', 'KAJOR BARU 14B JLN GODEAN KM 4', 'SMA BOPKRI 1', 'YOGYAKARTA', 1, 'L', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'PILEK', '-', '-', 'Tidak', '2016-06-21 13:38:54', '2016-06-21 15:04:24'),
(185, '31160021', 3, 'YUYUN ALVIONITA PABISA', 'Perempuan', 'NABIRE', '1998-07-28', 'B', 'Kristen', '081215076839', '082388757401', 'KALI HARAPAN, NABIRE', 'SAGAN', 'SMA NEGRI 3 NABIRE', 'NABIRE', 3, 'S', 'Tidak Ada Penyakit', '-', 'SESAK NAFAS', '-', '-', '-', 'OKSIGEN', 'Ya', '2016-06-21 13:47:12', '2016-06-21 15:03:01'),
(186, '72160021', 21, 'YONATAN', 'Laki-laki', 'Magelang', '1998-06-04', 'B', 'Kristen', '085879887888', '08886936067', 'JALAN KALINGGA 25 MAGELANG', 'BANGIREJO TR II 651A', 'SMA ELSHADAI', 'MAGELANG', 2, 'L', 'Tidak Ada Penyakit', '-', 'gejala demam berdarah', '-', '-', 'demam', 'jus jambu\r\n', 'Tidak', '2016-06-22 08:48:22', '2016-06-22 15:11:49'),
(187, '11160039', 29, 'WINI DALIANTI', 'Perempuan', 'SINTANG', '1998-03-21', 'O', 'Kristen', '089649708441', '082137354041', 'SINATNG KALIMANTAN BARAT', '-', 'SMA PANCA SETYA SINTANG', 'SINTANG KALIMATAN BARAT', 8, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'PERIH', 'PROMAGH', 'Ya', '2016-06-22 10:23:09', '2016-06-22 15:05:37'),
(188, '71160021', 26, 'BARTHOLOMEUS RYAN RIVALDO GARE', 'Laki-laki', 'Putussibau ', '1998-07-03', 'B', 'Katholik', '082148554506', '085245680618', 'Kab. Kapuas Hulu \r\nKota Putussibau ', 'Jln. Mawar No 14 Baciro ', 'Sma Karya Budi ', 'Putussibau ', 1, 'M', 'Tidak Ada Penyakit', '-', '-', 'Udang ', 'Gatal dan Timbul Bentolan ', '-', '-', 'Ya', '2016-06-22 11:31:52', '2016-06-22 15:10:38'),
(189, '61160043', 22, 'LEOANDA SENE PREMATURIO', 'Laki-laki', 'SAMARINDA', '1998-08-12', 'A', 'Kristen', '6285332022695', '6281346200957', 'JALAN RAJA PANDITA, MALINAU, KALIMANTAN UTARA', 'MAGUWOHARJO', 'SMA NEGERI 1 MALINAU', 'MALINAU, KALIMANTAN UTARA', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-22 11:51:21', '2016-06-22 15:09:49'),
(190, '61160016', 20, 'AUBREY CORNELIA ROSALIND SUBAGIO', 'Perempuan', 'YOGYAKARTA', '1998-03-22', 'A', 'Kristen', '6281903790393', '628121575850', 'JL. GODEAN, PERUM. GREEN GARDEN BLOK. D.11, YOGYAKARTA', 'JL. GODEAN, PERUM. GREEN GARDEN BLOK. D.11, YOGYAKARTA', 'SMA STELLA DUCE 1 ', 'YOGYAKARTA', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-22 11:56:31', '2016-06-22 15:09:22'),
(191, '11160003', 16, 'MICHAEL PIKANTO', 'Laki-laki', 'SLEMAN', '1997-10-21', 'A', 'Kristen', '62817274774', '0274374648', 'PANDEYAN UH5 779', 'JL PANDEYAN UH5 779', 'SMA BOPKRI SATU', 'YOGYAKARTA', 8, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-22 11:57:55', '2016-06-22 15:07:40'),
(192, '11160040', 33, 'VANIA YULIANTI', 'Perempuan', 'KUDUS', '1998-07-04', 'O', 'Katholik', '087833828895', '0878300281', 'JL.KUTILANG NO.47,KUDUS,JATENG', 'JL.IBIS,SITUMULYO, PIYUNGAN,PERUM CEPOKO INDAH D-07,RT06,BANTUL,YOGYAKARTA', 'SMA KANISIUS KUDUS', 'KUDUS', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-22 12:17:05', '2016-06-22 15:05:53'),
(193, '11160038', 7, 'NOVIANTO M DIO BERNAR', 'Laki-laki', 'SORONG', '1995-11-21', 'B', 'Katholik', '081289877594', '081248882478', 'JL DANAU ANGGI SORONG', 'JL SETURAN GAPURA PULUH DADI NO 421A', 'SMA YPPK AGUSTINUS', 'SORONG PAPUA', 8, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-22 12:30:03', '2016-06-22 15:06:44'),
(194, '11160041', 13, 'STEFANUS KEVIN SIRAIT', 'Laki-laki', 'TEGAL', '1998-08-11', '', 'Kristen', '083837282914', '081542047208', 'LARANGAN RT 003/ RW 007 BREBES', 'BABARSARI', 'SMA N 1 LARANGAN BREBES', 'BREBES', 8, 'XL', 'Tidak Ada Penyakit', '-', '-', 'UDANG', 'GATEL', '-', '-', 'Tidak', '2016-06-22 12:34:28', '2016-06-22 15:07:01'),
(195, '11160043', 15, 'MARKUS EDISON SIMBOLON', 'Laki-laki', 'PALEMBANG', '1998-04-25', 'A', 'Katholik', '081377523508', '082180007908', 'JLN. PERMAI NO.73', 'JL. Tutul no. 28, Papringan, Sleman', 'SMA XAVERIUS 1 ', 'PALEMBANG', 8, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-22 13:02:24', '2016-06-22 15:07:27'),
(196, '11160044', 24, 'REINHARD ISAAK AJOMI', 'Laki-laki', 'YOGJAKARTA', '1998-12-14', 'A', 'Kristen', '082199157537', '082199683064', 'JL.YAKONDE,NO 30,ABEPURA/JAYAPURA', 'NOLOGATEN', 'SMA NEGERI 1 JAYAPURA', 'JAYAPURA', 8, 'M', 'Tidak Ada Penyakit', '-', 'TIPES', '-', '-', 'DEMAM', '-', 'Tidak', '2016-06-22 13:26:36', '2016-06-22 15:07:54'),
(197, '01160019', 2, 'BAYU GUMELARING ADY', 'Laki-laki', 'JAKARTA', '1998-02-17', 'O', 'Kristen', '6282298899184', '6281318910588', 'KAVLING BUMI HARAPAN JAYA. JL. ANTARA I NO.55 BEKASI UTARA', '-', 'SMK BINA MANDIRI', 'BEKASI', 4, 'M', 'Tidak Ada Penyakit', '-', 'RADANG TENGGOROKAN', 'DURIAN', '-', 'FLU', 'ANTIBIOTIK', 'Ya', '2016-06-23 08:57:45', '2016-06-23 15:05:32'),
(198, '71160048', 6, 'HIERONIMUS NAROTAMA ', 'Laki-laki', 'POSO', '1999-02-08', 'O', 'Katholik', '6281228001931', '6282188923335', 'MINANGA INDAH BLOK A2 NO 16 MANADO SULUT', '-', 'SMA PANGUDI LUHUR YOGYAKARTA', 'YOGYAKARTA', 1, 'S', 'Tidak Ada Penyakit', '-', 'BRONKITIS', '-', '-', 'SESAK NAFAS', '-', 'Tidak', '2016-06-23 09:04:28', '2016-06-23 15:14:46'),
(199, '11160045', 1, 'MARIO STEVAN LEUNUPUN', 'Laki-laki', 'TERNATE', '1998-03-02', 'O', 'Kristen', '6281247500829', '6281247500855', 'SAUMLAKI', 'KLITREN', 'SMA NEGERI UNGGULAN ', 'SAUMLAKI', 8, 'M', 'Tidak Ada Penyakit', '- ', '-', 'MIE ', '- BINTIK-BINTIK DAN GATAL', '-', '- ANTIBIOTIK', 'Tidak', '2016-06-23 09:36:22', '2016-06-23 15:06:22'),
(200, '71160050', 7, 'FRANSISKUS DION ARYA DEWANGGA', 'Laki-laki', 'BANTUL', '1998-01-24', 'O', 'Katholik', '6281313421394', '6282119057931', 'PERUM.BINAGRAHA CILEUNYI WETAN', 'PADOKAN KIDUL TIRTONIRMOLO RT 04 RW 018', 'SMK MEDIKACOM', 'BANDUNG', 1, 'L', 'Tidak Ada Penyakit', 'DINGIN', '-', '-', 'KALIGATA (GATAL-GATAL)', '-', 'CTM', 'Tidak', '2016-06-23 09:56:42', '2016-06-23 15:15:04'),
(201, '41160025', 1, 'NADA DIAN SEJATI', 'Perempuan', 'KLATEN', '1998-01-30', 'A', 'Kristen', '085743934406', '081329034890', 'PERUMAHAN CIPTA GRIYA BERSINAR, A. 03, KALIKOTES, KLATEN', '-', 'SMA NEGERI 1 KLATEN', 'KLATEN', 10, 'S', 'Tidak Ada Penyakit', 'DINGIN DAN DEBU', '-', '-', 'GATAL DAN BERSIN', '-', 'AERIUS/ RISINA', 'Ya', '2016-06-23 10:27:24', '2016-06-23 15:10:32'),
(202, '71160020', 10, 'VINCENTIUS NICHOLAS HALIM', 'Laki-laki', 'YOGJAKARTA', '1997-11-24', 'B', 'Katholik', '085743556355', '08156805526', 'JALAN JAMBON 1 BLOK A TRI-414A, YOGYAKARTA', 'JALAN JAMBON 1 BLOK A TRI-414A, YOGYAKARTA', 'SMA KOLESE DE BRITTO', 'YOGYAKARTA', 1, 'XXXXL', 'Tidak Ada Penyakit', '-', 'ASMA, ', '-', '-', 'SESAK', '-', 'Ya', '2016-06-23 10:28:20', '2016-06-23 15:15:20'),
(203, '41160031', 32, 'PUTU TIRZY ENJELICA', 'Perempuan', 'SINGARAJA', '1999-07-25', 'B', 'Kristen', '081999190590', '081805647227', 'JALAN SANGHYANG, BUDUK, KUTA UTARA, BADUNG, BALI', 'JALAN DR WAHIDIN SUDIROHUSODO NO 60', 'SMA NEGERI 1 KUTA UTARA', 'BADUNG', 10, 'S', 'Tidak Ada Penyakit', '-', '-', 'MSG BERLEBIHAN', 'BINTIK BINTIK MERAH', '-', '-', 'Ya', '2016-06-23 10:35:03', '2016-06-23 15:10:50'),
(204, '41160011', 30, 'PUTU CLARA SHINTA GELGEL', 'Perempuan', 'JAKARTA', '1997-04-19', 'O', 'Katholik', '085878180168', '0811767472', 'JALAN CENDANA NO 39 KPR 1, PERAWANG, TUALANG, SIAK, RIAU,INDONESIA', 'JALAN MUNGGUR GG SRIKANDI NO 8, GONDOKUSUMAN,KOTA YOGYA, DIY', 'SMA STELLA DUCE 1 ', 'YOGYAKARTA', 10, 'S', 'Tidak Ada Penyakit', 'DEBU\r\nDINGIN', 'ASMA DAN SINUSITIS', 'BUAH PEAR\r\nKERANG', 'BIBIR JADI PANAS, GATEL (MAKANAN)\r\nBERSIN-BERSIN (DEBU, DINGIN)', 'SESAK, PILEK', 'RHINOFED (SINUS, PILEK)', 'Tidak', '2016-06-23 10:38:16', '2016-06-23 15:11:02'),
(205, '12160032', 8, 'SISILIA VEBRIYANTI YENI NDELO', 'Perempuan', 'WAIKABUBAK', '1998-02-17', '', 'Katholik', '082340622098', '085238022937', 'WANNO RONGO,WATUKAWULA,SUMBA BARAT DAYA,NTT', '-', 'SMAS ST THOMAS AQUINAS', 'WEETEBULA', 7, 'S', 'Tidak Ada Penyakit', '-', 'BENGKAK LEHER', '-', '-', '-', '-', 'Ya', '2016-06-23 10:54:11', '2016-06-23 15:08:29'),
(206, '71160051', 11, 'DRIYANDO ANDREAS TAMBUNAN', 'Laki-laki', 'TITIAN RESAK', '1998-04-20', 'O', 'Kristen', '081261733680', '085278227493', 'TITIAN RESAK RT 19 RW 05 BELILAS, INDRAGIRI HULU, RIAU', 'IROMEJAN GK3 733 KLITREN GONDOKUSUMAN YOGYAKARTA', 'SMA NEGERI 1 SEBERIDA', 'RENGAT', 1, 'M', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'GATAL GATAL', '-', 'KETENANGAN', 'Ya', '2016-06-23 11:42:55', '2016-06-23 15:15:34'),
(207, '11160046', 10, 'MELATI NOVIANTIKA', 'Perempuan', 'ANTAN', '1998-10-02', 'O', 'Kristen', '082350764202', '082153950893', 'DUSUN ANTAN ,KECAMATAN NGABANG ,KABUPATEN LANDAK ,PROVINSI KALIMANTAN BARAT', 'WIDORO BARU,JALAN ARU NO 2, CONDONG CATUR SLEMAN YOGYAKARTA', 'SMA SANTO PAULUS', 'SINGKAWANG', 8, 'M', 'Tidak Ada Penyakit', '-', '-', 'AYAM', 'GATAL GATAL', '-', '-', 'Tidak', '2016-06-23 11:46:20', '2016-06-23 15:07:23'),
(208, '12160033', 9, 'APRISTENCY GADUNG', 'Perempuan', 'LEMBUDUD', '1998-04-10', 'B', 'Kristen', '085346889253', '082254797456', 'LEMBUDUD KALIMANTAN UTARA', 'PUGERAN', 'SMAN 1 KRAYAN', 'LONG BAWAN', 7, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-23 11:51:59', '2016-06-23 15:08:48'),
(209, '12160034', 10, 'IRA GRACELYA LASANI', 'Perempuan', 'BETELEME', '1998-09-20', '', 'Kristen', '081214014670', '085396867073', 'BETELEME, KAB. MOROWALI UTARA SULAWESI TENGAH', 'JL. YUDISTIRA NO 6 NGILINGGAN RT 5 RW 3 NGEMPLAK WEDOMARTANI SLEMAN YOGYAKARTA', 'SMA NEGERI 1 LEMBO', 'LEMBO', 7, 'S', 'Tidak Ada Penyakit', 'UDARA DINGIN', '-', '-', 'GATAL-GATAL', '-', '-', 'Tidak', '2016-06-23 12:12:49', '2016-06-23 15:09:04'),
(210, '41150086', 27, 'AVE MARIA ROSARIO', 'Perempuan', 'METRO', '1997-09-04', 'A', 'Kristen', '082226297031', '0811796877', 'JL. HASANUDIN NO.122 YOSOMULYO KOTA METRO, LAMPUNG', 'KOMPLEKS LPSS SINODE NO. 72 SAMIRONO BARU', 'SMA KRISTEN 1 METRO', 'KOTA METRO ', 10, 'empty', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-23 12:16:58', '2016-06-23 15:11:55'),
(211, '11160048', 11, 'MARYANTI SHERLIYANA DARUNG', 'Perempuan', 'BERU', '1998-11-14', 'AB', 'Katholik', '085338310348', '081339494372', 'JL.BERINGIN RT/RW 004/003 KEL.NANGAMETING KEC. ALOK TIMUR', '-', 'SMAK BHAKTYARSA MAUMERE', 'MAUMERE', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-23 12:35:20', '2016-06-23 15:07:49'),
(212, '61160045', 34, 'ALOYSIUS EFRAEM NOFARIO', 'Laki-laki', 'KULON PROGO', '1998-03-16', '', 'Katholik', '085292330802', '08121553947', 'TEGALSARI,PURWOSARI,GIRIMULYO,KULON PROGO', 'TEGALSARI,PURWOSARI,GIRIMULYO,KULON PROGO', 'SMK PELITA BANGSA', 'YOGYAKARTA', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-23 12:51:18', '2016-06-23 15:12:59'),
(213, '61160044', 17, 'YORLAN SAPUTRA TUNGGELE', 'Laki-laki', 'TAENDE', '1997-04-28', '', 'Kristen', '082293567908', '085340205510', 'DESA TAENDE, KECAMATAN MORI ATAS, KAB. MOROWALI UTARA, PROV. SULTENG', 'KAB. SLEMAN, JLN. CONDONG CATUR', 'SMA N 2 ', 'POSO', 6, 'S', 'Tidak Ada Penyakit', '-', 'RABUN JAUH', '-', '-', '-', '-', 'Tidak', '2016-06-23 12:54:04', '2016-06-23 15:13:20'),
(214, '31160022', 15, 'YOSEPH JUNEDI NUWA DHUGE POA', 'Laki-laki', 'MAUMERE', '1998-06-25', 'O', 'Katholik', '082236550131', '085231122319', 'JALAN DON DJUAN KOTAUNENG MAUMERE, FLORES NTT\r\nRT: 001/RW:004', 'JALAN NANGKA 2 DEPOK SLEMAN', 'SMAK FRATERAN MAUMERE', 'MAUMERE', 3, 'M', 'Tidak Ada Penyakit', 'PANAS MATAHARI', 'AUTOIMUN (SLE)', 'MAKANAN PENGAWET (INDOMIE)', 'BADAN MEMERAH DAN GATAL-GATAL', 'KRAM PADA PERSENDIAN KAKI DAN TANGAN, MUAL DAN MUNTAH', 'METHIL PREDISOLON, 16 MG\r\nLANSOPLAZOLE', 'Ya', '2016-06-23 13:04:42', '2016-06-23 15:10:06'),
(215, '71160047', 13, 'EFI OKTAVIANI', 'Perempuan', 'RIAN', '1997-07-06', '', 'Katholik', '081348581594', '085389938511', 'JL.PELAJAR II RT 002 ,KAPUAK ,KALIMANTAN UTARA', 'JL.KEMIRI NOLOGATEN NO 159', 'SMA NEGERI 1 ', 'MALINAU', 1, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-23 13:04:52', '2016-06-23 15:14:27'),
(216, '61160046', 13, 'JULIVIO ANDRE BULAU', 'Laki-laki', 'PONTIANAK', '1998-07-15', 'A', 'Katholik', '085822750653', '085822176160', 'JALAN 28 OKTOBER ,KOMPLEK DWI RATNA INDAH NO 11C', 'JALAN WAHID HASYIM ,GANG KANTIL NO 170D', 'SMA SANTO PAULUS', 'PONTIANAK', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-23 13:23:38', '2016-06-23 15:13:34'),
(217, '61160047', 21, 'NENGSI', 'Perempuan', 'BALIKU', '1997-10-25', 'B', 'Kristen', '082251626787', '081350849300', 'TANJUNG LAPANG, RT 011, TANJUNG LAPANG, MALINAU BARAT', 'ASRAMA MALINAU', 'SMA NEGERI 1 MALINAU', 'MALINAU', 6, 'S', 'Tidak Ada Penyakit', 'DINGIN', 'MAAG BIASA', '-', 'GATAL-GATAL', 'SAKIT PERUT', '-', 'Ya', '2016-06-23 13:55:47', '2016-06-23 15:12:48'),
(218, '11160005', 18, 'NIKEN AUDITIA PRATIWI', 'Perempuan', 'PALEMBANG', '1998-04-27', 'O', 'Kristen', '087838931405', '082138252004', 'PALEMBANG JL.KEMANG MANIS', 'JL.KADIPATEN KULON', 'SMA BOPKRI 2 YOGYAKARTA', 'YOGYAKARTA', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', 'GEJALA TIPUS', '-', 'PILEK, SESAK', 'LELAH', 'VERMINT', 'Ya', '2016-06-23 13:55:55', '2016-06-23 15:08:03'),
(219, '71160019', 14, 'ANDREW LIMAWAN HERRISON', 'Laki-laki', 'TARAKAN', '1997-07-20', 'B', 'Kristen', '+6285386842997', '+628125829846', 'JALAN FLAMBOYAN RT. 27 NOMOR 53 KARANG ANYAR, KECAMATAN TARAKAN BARAT 77111', '-', 'SMA KRISTEN TUNAS KASIH ', 'TARAKAN', 1, 'L', 'Tidak Ada Penyakit', 'DEBU, DINGIN, ASAP', 'ASMA, DEMAM BERDARAH', '-', 'DEBU = BERSIN-BERSIN\r\nDINGIN = BERSIN-BERSIN\r\nASAP = SESAK NAPAS, LEMAS', 'ASMA = SESAK NAPAS, LEMAS', 'VENTOLIN INHALER', 'Tidak', '2016-06-23 14:31:47', '2016-06-23 15:15:57'),
(220, '71160052', 23, 'NATANIEL DOMINUS RIVALDI', 'Laki-laki', 'SERUKAM', '1998-12-18', 'B', 'Kristen', '081250213196', '081352266603', 'DUSUN PULAU BENDU KECAMATAN NGABANG KABUPATEN LANDAK PROVINSI KALIMANTAN BARAT.', '-', 'SMA NEGERI 1 NGABANG', 'NGABANG ', 1, 'L', 'Tidak Ada Penyakit', 'DINGIN', '-', '-', 'BERSIN BERSIN', '-', '-', 'Ya', '2016-06-23 14:38:55', '2016-06-23 15:16:12'),
(221, '71160018', 9, 'THOMAS WIDIARYA BUDIMAN', 'Laki-laki', 'PEKALONGAN', '1998-10-05', 'O', 'Kristen', '089521337118', '081802870007', 'PERUMAHAN PALAPA 4 NO 16', 'JALAN BANGIREJO TAMAN NO 22', 'SMAN 3 PEKALONGAN', 'PEKALONGAN', 1, 'L', 'Tidak Ada Penyakit', '-', '-', 'MAKANAN LAUT', 'GATEL-GATEL', '-', '-', 'Tidak', '2016-06-24 09:10:05', '2016-06-24 15:22:18'),
(222, '61160002', 3, 'ALVIN PRATAMA PUTRA', 'Laki-laki', 'YOGYAKARTA', '1999-03-29', 'AB', 'Kristen', '082314131206', '08122784123', 'JL. GODEAN PERUMAHAN GRIYA ARGA PERMAI GANG MERAPI N.1 , YOGYAKARTA', 'JL. GODEAN PERUMAHAN GRIYA ARGA PERMAI GANG MERAPI N.1 , YOGYAKARTA', 'SMA SANTO BERNARDUS ', 'PEKALONGAN', 6, 'M', 'Tidak Ada Penyakit', 'ALERGI OBAT', 'MAAG', '-', 'JIKA ADA KANDUNGAN PINISILIN AKAN TERJADI MATA BENGKAK ', 'ULU HATI SAKIT', 'PROMAAG', 'Ya', '2016-06-24 09:12:37', '2016-06-24 15:19:02'),
(223, '11160049', 20, 'EGGA PUTRA KRISTIAN', 'Laki-laki', 'LABUHAN MARINGGAI', '1997-06-19', '', 'Kristen', '085783357278', '085266406452', 'KARYA TANI LABUHAN MARINGGA,LAMPUNG TIMUR', 'RT 6 RW 7 KAMPUNG MANUNGGALAN PENDUKUHAN PONDOK DESA CONDONG CATUR DEPOK', 'SMA NEGERI 1 PASIR SAKTI', 'BANDAR LAMPUNG', 8, 'M', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'ULU HATI SAKIT', 'PROMAG', 'Tidak', '2016-06-24 09:23:28', '2016-06-24 15:12:55'),
(224, '11160008', 25, 'FRENDY PRATAMA', 'Laki-laki', 'KARYA TANI', '1998-09-01', '', 'Kristen', '085268299301', '082282533835', 'DUSUN III RT 007 RW 004 KARYA MAKMUR LABUHAN MARINGGAI,LAMPUNG TIMUR', 'RT 06 RW 07 KAMPUNG MANGGULAN PENDUKUHAN PONDOK DESA CONDONG CATUR DEPOK', 'SMA NEGERI 1 PASIR SAKTI', 'LAMPUNG', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'BERSIN-BERSIN', '-', '-', 'Ya', '2016-06-24 09:24:19', '2016-06-24 15:13:32'),
(225, '11160010', 34, 'ANDRIANITA CHRISTIANINGRUM', 'Perempuan', 'BANTUL', '1998-03-20', 'O', 'Kristen', '083840413318', '08156898542', 'SIDOKERTO RT02/RW01, PURWOMARTANI, KALASAN, SLEMAN', 'SIDOKERTO RT02/RW01, PURWOMARTANI, KALASAN, SLEMAN', 'SMA BOPKRI 2 YOGYAKARTA', 'YOGYAKARTA', 8, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'SESAK, BERSIN, PILEK', '-', '-', 'Ya', '2016-06-24 09:29:47', '2016-06-24 15:14:34'),
(226, '72160022', 28, 'DAVID FEBRIAWAN PRADANA', 'Laki-laki', 'KLATEN', '1997-02-09', 'B', 'Kristen', '6285747876762', '6281226306681', 'JATIWOYO, MAYUNGAN NGAWEM,KLATEN', '-', 'SMK LEONARDO KLATEN', 'KLATEN', 2, 'M', 'Tidak Ada Penyakit', 'DINGIN', '-', '-', 'KULIT MERAH', '-', '-', 'Ya', '2016-06-24 09:40:39', '2016-06-24 15:26:09'),
(227, '71160049', 8, 'JOHN A. LODARMAS', 'Laki-laki', 'SERUI', '1998-04-07', 'O', 'Kristen', '082398753619', '085254957622', 'KAMPUNG KAINUI II DISTRIK ANGKAISERA', 'JL. DR WAHIDIN NO.16', 'SMK YPK SERUI', 'SERUI', 1, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-24 09:54:33', '2016-06-24 15:22:37'),
(228, '61160007', 4, 'DANNY HIDAYAT', 'Laki-laki', 'SALATIGA', '1998-07-04', 'O', 'Islam', '085729319128', '085643408423', 'NGABLAK RT 5 RW 5 KEL.PULUTAN KEC.SIDOREJO KOTA.SALATIGA', '-', 'SMK NEGERI 2 SALATIGA', 'SALATIGA', 6, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-24 10:02:58', '2016-06-24 15:19:14'),
(229, '61160048', 14, 'CINDY GRASELLA LAAMENA', 'Perempuan', 'PASSO', '1998-07-25', 'O', 'Kristen', '085244438665', '085257401416', 'JL.BOBARA NO.58,TUAL', 'JL KLITREN', 'SMA SANATA KARYA LANGGUR', 'MALUKU TENGGARA', 6, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', 'MAKANAN LAUT (UDANG)', 'MAKANAN: GATAL-GATAL\r\nDEBU: SESAK NAFAS', '-', '-', 'Ya', '2016-06-24 10:05:59', '2016-06-24 15:20:09'),
(230, '12160035', 31, 'TOMGILBERTH KENDEKALLO', 'Laki-laki', 'RANTEPAO', '1998-03-15', 'O', 'Kristen', '082257578968', '081238310898', 'LINGKUNGAN BUNGA TANAH', 'JLN.GATAK,, YOGYAKARTA', 'SMAN 1 RANTEPAO', 'RANTEPAO', 7, 'S', 'Tidak Ada Penyakit', 'DINGIN', 'MAAG', '-', 'KULIT MERAH ', 'SAKIT DI ULU HATI', '-', 'Ya', '2016-06-24 10:26:32', '2016-06-24 15:17:37'),
(231, '12160036', 16, 'MICHAEL DYAS PERDANA', 'Laki-laki', 'TEGAL', '1996-06-19', 'A', 'Kristen', '6289685148410', '6281575319607', 'JALAN PALA RAYA NO 5A MEJASEM BARAT, KAB. TEGAL', 'KEPUH GK.3/1062 A YOGYAKARTA', 'SMA PIUS TEGAL', 'TEGAL', 7, 'S', 'Tidak Ada Penyakit', 'UDARA DINGIN', 'JANTUNG BAWAAN', '-', 'JANTUNG NYERI', 'NYERI DI JANTUNG', 'BISOPROLOL', 'Tidak', '2016-06-24 10:38:26', '2016-06-24 15:16:59'),
(232, '01160021', 19, 'FILO YUSTIKARNO KRISTYA PERSADA', 'Laki-laki', 'SURAKARTA', '1998-01-25', 'O', 'Kristen', '082226315352', '08122630593', 'JL ADISUCIPTO 135 RT 04/ RW 09, KARANGASEM, LAWEYAN, SURAKARTA', 'ASRAMA TEOLOGI', 'SMKN 8 SURAKARTA', 'SURAKARTA', 4, 'M', 'Tidak Ada Penyakit', '-', '-', 'UDANG', '-', '-', '-', 'Ya', '2016-06-24 11:19:38', '2016-06-24 15:11:10'),
(233, '62160002', 18, 'BINTANG HENRI TRIJAYA', 'Laki-laki', 'YOGYAKARTA', '1998-11-02', 'O', 'Kristen', '08386868068', '02742870507', 'KEPUH GK III', 'KUSUMANEGARA 118', 'SMA BOPKRI 1', 'YOGYAKARTA', 5, 'XXXL', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'BERSIN, SESAK', '-', '-', 'Ya', '2016-06-24 11:21:43', '2016-06-24 15:21:06'),
(234, '11160050', 31, 'DELLA ERICASARDI', 'Perempuan', 'SINTANG', '1998-03-28', 'AB', 'Budha', '089630385439', '085249304560', 'JL KWEE JIU HOI, G.G ANEKA BARU NO 5', 'JL KLITREN LOR GK NO 423', 'SMA PANCA SETYA', 'SINTANG', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-24 11:25:36', '2016-06-24 15:14:45'),
(235, '12160009', 21, 'MELLYA JOANNITA SITORINI', 'Perempuan', 'SLEMAN', '1997-11-18', 'A', 'Kristen', '628985798653', '6285102096079', 'JL.ADISUMARMO NO.123 SURAKARTA ', 'JL.KENDALISOO NO.13A GEDONG KUNING', 'SMA BOPKRI 1 YOGYAKARTA', 'YOGYAKARTA', 7, 'S', 'Tidak Ada Penyakit', 'OBAT - PENICILLIN', 'MAAG', '-', 'PANAS', 'PINGSAN', 'METAFLAM', 'Ya', '2016-06-24 11:29:03', '2016-06-24 15:15:38'),
(236, '71160014', 24, 'YONATAN KRISTANTO', 'Laki-laki', 'SEMARANG', '1998-06-02', 'B', 'Kristen', '089625873621', '08122712200', 'JL.RAJAWALI NO 80B SLEMAN,YOGYAKARTA', 'JL.RAJAWALI NO 80B SLEMAN,YOGYAKARTA', 'SMA BOPKRI SATU', 'YOGYAKARTA', 1, 'L', 'Tidak Ada Penyakit', '-', '-', 'JEROAN', '-', '-', '-', 'Ya', '2016-06-24 11:30:28', '2016-06-24 15:24:09'),
(237, '71160016', 15, 'SYLVIA PUTRI REJEKI GUNAWAN', 'Perempuan', 'WONOSOBO', '1998-02-07', 'B', 'Katholik', '085740538060', '085328283888', 'PERUMAHAN ASLI PERMAI BLOK O NO.18 RT 8 RW 5, KRAMATAN, WONOSOBO, JAWA TENGAH', '-', 'SMA NEGERI 1 WONOSOBO', 'WONOSOBO', 1, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-24 11:37:09', '2016-06-24 15:24:21'),
(238, '61160001', 5, 'VINCENT FANUEL GEDALYA', 'Laki-laki', 'PEMALANG', '1998-02-02', 'O', 'Kristen', '085742331816', '085742328400', 'PEMLANG,PERUMAHAN ALAM WIDURI ASRI BLOK J NO 25', 'KLITREN LOR GK=3 NO=366', 'SMA NEGERI 2 PEMALANG', 'PEMALANG', 6, 'XXL', 'Tidak Ada Penyakit', '-', 'TIPES', 'SEMUA MAKANAN LAUT, SUNGAI .', 'MUNTAH, GATAL', 'PANAS,PERUTNYA SAKIT', '-', 'Ya', '2016-06-24 11:41:55', '2016-06-24 15:19:27'),
(239, '12160002', 22, 'SEPTILIA SEKAR MURDISARI', 'Perempuan', 'TEGAL', '1997-09-24', 'A', 'Kristen', '628974036747', '6285866839911', 'JALAN PALA RAYA NO 5A RT02/RW05\r\nMEJASEM BARAT \r\nKEC.KRAMAT\r\nKAB.TEGAL', 'KEPUH GK 3/1062 A YOGYAKARTA', 'SMA N 4 TEGAL', 'TEGAL', 7, 'S', 'Tidak Ada Penyakit', 'UDARA DINGIN , ASAP ROKOK', 'ASMA', '-', 'SESAK NAFAS', 'SESAK NAFAS', 'SALBUTAMOL', 'Tidak', '2016-06-24 11:47:11', '2016-06-24 15:16:03'),
(240, '72160023', 27, 'VINCENSIUS APAULO ALFIAN LEWA ARUM', 'Laki-laki', 'SOE', '1996-04-04', '', 'Katholik', '081313028117', '081252301813', 'KEL.KARANG SIRI RT.010 RW.004 KEC.KOTA SOE', 'SEKITAR UPN', 'SMK YOS SUDARSO SOKARAJA', 'PURWOKERTO', 2, 'XL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-24 12:08:54', '2016-06-24 15:26:21'),
(241, '71160053', 30, 'BOB ANDRESON SETIADY', 'Laki-laki', 'JAKARTA', '1997-04-28', 'A', 'Katholik', '08176593170', '08176593170', 'PERUM VILLA MELATI MAS BLOK P3 NO 24, SERPONG, TANGERANG SELATAN, BANTEN', 'PERUM GRIYA MRISI INDAH BLOK D2, KASIHAN, BANTUL', 'SMK BOPKRI 1', 'YOGYAKARTA', 1, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-24 13:16:11', '2016-06-24 15:24:47'),
(242, '11160051', 26, 'REYNOLD RINALDY IKHSAN', 'Laki-laki', 'BANJARMASIN', '1997-06-30', 'AB', 'Kristen', '6282154926306', '6281351299968', 'DATU BALIMBINGAN,KAB.BALANGAN KALIMANTAN SELATAN', 'JLN. WAHID HASYIM GANG MENUR NO.103C KOST PUTRA D\'TWINZ CATURTUNGGAL, DEPOK YOGYAKARTA', 'SMA FRATER DON BOSCO', 'BANJARMASIN', 8, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-24 13:42:13', '2016-06-24 15:13:53'),
(243, '61160005', 12, 'RAYEN MOORE LUBIS', 'Laki-laki', 'CIREBON', '1998-09-26', 'B', 'Kristen', '081949032904', '0818238088', 'JL. PILANG SETRAYASA RAYA NO.8, KOTA CIREBON, JAWA BARAT', 'JL.KAYEN', 'SMAN 2 CIREBON', 'CIREBON', 6, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-24 13:53:30', '2016-06-24 15:19:52'),
(244, '41160012', 16, 'IKA IRMAWATI SUSANTO', 'Perempuan', 'MANOKWARI', '1998-01-06', 'B', 'Kristen', '6285742542374', '6287733956878', 'DESA GADUDERO 04/01, KECAMATAN SUKOLILO, KABUPATEN PATI', 'JALAN LANGENSARI 19, YOGYAKARTA', 'SMAN 2 PATI', 'PATI', 10, 'M', 'Tidak Ada Penyakit', '-', 'HNP', '-', '-', 'NYERI', '-', 'Tidak', '2016-06-27 08:21:42', '2016-06-27 15:09:12'),
(245, '31160002', 2, 'DYAH KUSUMASARI', 'Perempuan', 'SLEMAN', '1999-04-23', 'O', 'Islam', '62812716554', '628806020590', 'JALAN LAMPAR NO. 2, PAPRINGAN, YOGYAKARTA', 'JALAN LAMPAR NO. 2, PAPRINGAN, YOGYAKARTA', 'SMA STELLA DUCE 2 YOGYAKARTA', 'YOGYAKARTA', 3, 'S', 'Tidak Ada Penyakit', '-', 'SESAK NAFAS', '-', '-', 'SESAK', 'ISTIRAHAT', 'Tidak', '2016-06-27 10:19:01', '2016-06-27 15:08:21'),
(246, '31160008', 5, 'DEBORA ALFI SUNARYA', 'Perempuan', 'YOGYAKARTA', '1998-06-07', 'B', 'Kristen', '687839386471', '68175491625', 'KUMENDAMAN MJ 2 / 596 A ', 'KUMENDAMAN MJ 2 / 596 A ', 'SMA STELLA DUCE 2', 'YOGYAKARTA', 3, 'L', 'Tidak Ada Penyakit', '-', '-', 'Makanan berlemak', 'ADA CAIRAN DI BEKAS LUKA', '-', 'GENTAMICIN', 'Tidak', '2016-06-27 10:19:08', '2016-06-27 15:08:34'),
(247, '71160002', 21, 'ALVIN WICAKSONO', 'Laki-laki', 'YOGYAKARTA', '1998-06-19', 'O', 'Kristen', '6289630006003', '6281392870304', 'PERUM PARIS GRAHAYASA 1 / E-2 BANGUNHARJO SEWON BANTUL YOGYAKARTA', 'PERUM PARIS GRAHAYASA 1 / E-2 BANGUNHARJO SEWON BANTUL YOGYAKARTA', 'SMA PANGUDI LUHUR', 'YOGYAKARTA', 1, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-27 10:23:41', '2016-06-27 15:23:38'),
(248, '72160004', 3, 'FELICITAS GEOVANI PRASETYA', 'Laki-laki', 'YOGYAKARTA', '1998-03-24', 'O', 'Katholik', '628984485636', '6285879940317', 'JALAN ONTOREJO NO. 1 WIROBRAJAN YOGYAKARTA', 'JALAN ONTOREJO NO.1 WIROBRAJAN YOGYAKARTA', 'SMA PANGUDI LUHUR YOGYAKARTA', 'YOGYAKARTA', 2, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-27 10:24:49', '2016-06-27 15:24:34'),
(249, '61160049', 30, 'ANGELA EVITA KURNIASARI', 'Perempuan', 'BANTUL', '1998-05-09', 'O', 'Katholik', '6285728491598', '6281325972739', 'JANGGAN RT05 KANUTAN SUMBERMULYO BAMBANGLIPURO BANTUL YOGYAKARTA', 'JANGGAN RT05 KANUTAN SUMBERMULYO BAMBANGLIPURO BANTUL YOGYAKARTA', 'SMA N 1 PAJANGAN ', 'YOGYAKARTA', 6, 'XXXL', 'Tidak Ada Penyakit', '-', 'ASAM LAMBUNG', '-', '-', 'SAKIT PERUT', 'AIR ANGET, JAHE', 'Ya', '2016-06-27 10:43:59', '2016-06-27 15:10:36'),
(250, '11160052', 29, 'I MADE JUNIOR ANJASMARA', 'Laki-laki', 'DENPASAR', '1999-04-25', 'O', 'Hindu', '62817342426', '6281999155355', 'JALAN PANTAI KUTA NO 36', 'JALAN COLOMBO', 'SMA TAMAN RAMA', 'BADUNG', 8, 'M', 'Tidak Ada Penyakit', 'DEBU', 'MAAG, ', '-', 'GATAL GATAL', 'PERUT SAKIT', 'MYLANTA', 'Tidak', '2016-06-27 10:48:39', '2016-06-27 15:07:19'),
(251, '41160046', 32, 'JULIAN MATIUS SAHALA SILITONGA', 'Laki-laki', 'JAKARTA', '1999-07-28', 'A', 'Kristen', '082280510107', '081328842471', 'PERUMAHAN BATANG HARI PERMAI 1,NO 38,BENGKULU', 'PURI MOJO ASRI,SLEMAN,YOGYAKARTA', 'SMAN 02 KOTA BENGKULU', 'KOTA BENGKULU', 10, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-27 11:21:10', '2016-06-27 15:09:52'),
(252, '11160042', 4, 'LISA AMANDANI', 'Perempuan', 'YOGYAKARTA', '1998-04-03', 'O', 'Kristen', '085729927010', '08122763072', 'PONOWAREN 16/19 NOGOTIRTO , GAMPING SLEMAN', 'PONOWAREN 16/19 NOGOTIRTO , GAMPING SLEMAN', 'SMA BOPKRI 1 YOGYAKARTA', 'YOGYAKARTA', 8, 'M', 'Tidak Ada Penyakit', '-', '-', 'UDANG', 'MERAH MERAH, GATEL', '-', 'DIBIARKAN', 'Ya', '2016-06-27 11:56:32', '2016-06-27 15:07:44'),
(253, '71160012', 19, 'ESPERANTISTA ISA SAMIAJI GUNAWAN', 'Laki-laki', 'SLEMAN', '1998-04-03', 'A', 'Kristen', '083848701242', '08159917294', 'Gg RAJAWALI RT 29 RW 08 NO 636, TEGALREJO, YOGYAKARTA', 'Gg RAJAWALI RT 29 RW 08 NO 636, TEGALREJO, YOGYAKARTA', 'SMAN 2 YOGYAKARTA', 'YOGYAKARTA', 1, 'XL', 'Tidak Ada Penyakit', 'DEBU, DINGIN', 'RADANG TENGGOROKAN', '-', 'PILEK', 'SUHU BADAN NAIK', 'TROCHES', 'Ya', '2016-06-27 12:06:12', '2016-06-27 15:23:15'),
(254, '62160008', 3, 'OLIVIA ERWIMA', 'Perempuan', 'SURAKARTA', '1998-09-09', 'B', 'Katholik', '0895331498808', '087835113330', 'JL MARGOREJO TIMUR RT01/RW12 GILINGAN BANJARSARI SURAKARTA', '-', 'SMA PANGUDI LUHUR SANTO YOSEF SURAKARTA ', 'SURAKARTA', 5, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', '-', 'BERSIN, PILEK', '-', '-', 'Ya', '2016-06-27 12:22:54', '2016-06-27 16:05:00'),
(255, '61160050', 33, 'BILLY JOSEPH SALAMPESSY', 'Laki-laki', 'SORONG', '1998-10-22', 'O', 'Kristen', '081354172110', '082399955550', 'JALAN MALIBELA KM. 11,5 SORONG PAPUA BARAT', '-', 'SMA YPPKK MORIA KOTA SORONG', 'SORONG PAPUA BARAT', 6, 'XL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-27 12:26:39', '2016-06-27 15:10:49'),
(256, '61160052', 1, 'NOEL FRANCISCO CORREIA', 'Laki-laki', 'DILI TIMOR LESTE', '1997-03-07', '', 'Katholik', '08227199708', '+67077297402', 'BECORA DILI TIMOR LESTE', 'JALAN BENER TEGAL REJO IV/120 RT/RW 009/003 YOGYAKARTA', 'ESCOLA TECNICA INFORMATICA', 'DILI', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-27 14:12:11', '2016-06-27 16:03:39'),
(257, '41160047', 18, 'DEWA KETUT KARTIKA PUTRA', 'Laki-laki', 'Tabanan', '1998-04-21', 'O', 'Hindu', '6285739750750', '6285857599205', 'JALAN WARKUDARA GANG XX NO.2 TABANAN BALI', 'KLITREN', 'SMAN 1 TABANAN', 'BALI', 10, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-28 09:40:33', '2016-06-28 15:05:11'),
(258, '11160047', NULL, 'GETFOND GUSTO', 'Laki-laki', 'KENDARI', '1996-09-06', 'AB', 'Kristen', '6287787264499', '6281288979988', 'JL.PINANG RAYA BLOK AS51 N0.09', 'JALAN JAWA CONDONG CATUR', 'SMA KARTIKA-VII-2', 'KENDARI', 8, 'M', 'Tidak Ada Penyakit', '-', '-', 'CUMI-CUMI', 'GATAL-GATAL', '-', 'CAPSUL', 'Tidak', '2016-06-28 10:19:18', '2016-06-28 10:19:18'),
(259, '31160010', 18, 'YOLLANDA TIFFANY SUSABDA', 'Perempuan', 'MAGELANG', '1997-12-30', 'O', 'Kristen', '081903765558', '081945575758', 'JL. RAJAWALI 3 NO. B15 KLASEMAN CONDONGCATUR', 'JL. RAJAWALI 3 NO. B15 KLASEMAN CONDONGCATUR', 'SMA BOPKRI 1', 'YOGYAKARTA', 3, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-28 10:59:01', '2016-06-28 15:04:29'),
(260, '71160043', 18, 'GILBERT LEONARDO KADAKOLO', 'Laki-laki', 'JAYAPURA', '1998-02-14', '', 'Kristen', '081220532036', '082399783737', 'JL. TRIKORA . KAMPUNG SALAK. SORONG, PAPUA BARAT', 'JL. GEMPOL RAYA , PERUM. GRIYA PESONA MULIA , 3E', 'SMA SANTO THOMAS', 'YOGYAKARTA', 1, 'L', 'Tidak Ada Penyakit', '-', '-', 'IKAN TENGIRI', 'PANAS DI LIDAH', '-', 'MINUM AIR PUTIH', 'Ya', '2016-06-28 11:06:15', '2016-06-28 15:06:31'),
(261, '41160038', 24, 'NOKI OTTO KRISTANTO', 'Laki-laki', 'KLATEN', '1998-02-23', 'B', 'Kristen', '081215057171', '081392399394', 'MANGUNAN, KANOMAN, KARANGNONGKO, KLATEN', '- ', 'SMAN 2 KLATEN', 'KLATEN', 10, 'L', 'Tidak Ada Penyakit', 'ALERGI DINGIN ', '- OPERASI LANGIT - LANGIT MULUT', '- ', 'SESAK NAFAS, TENGGOROKAN SAKIT, PUSING', ' - ', 'MENGGUNAKAN JAKET, MENUNGGU CUACA SAMPAI TIDAK DINGIN', 'Tidak', '2016-06-28 12:28:14', '2016-06-28 15:05:38');
INSERT INTO `pesertas` (`id`, `nim`, `idkelompok`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `golongan_darah`, `agama`, `nomor_telpon`, `nomor_orangtua`, `alamat_asal`, `alamat_jogja`, `asal_sekolah`, `kota_asal_sekolah`, `idprodi`, `ukuran_almamater`, `jenis_penyakit`, `alergi`, `penyakit_diderita`, `alergi_makanan`, `ralergi`, `rpenyakit`, `obat`, `tonti`, `created`, `modified`) VALUES
(262, '11160053', NULL, 'YOHANES ROBI', 'Laki-laki', 'MANSIK', '1996-09-07', 'A', 'Kristen', '085750386173', '081545912325', 'DUSUN BUNDANG,DESA KUPAN JAYA,KAB.SINTANG,KALBAR', '-', 'SMA NEGERI 2 SINTANG', 'KAB.SINTANG', 8, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-28 12:36:19', '2016-06-28 12:36:19'),
(263, '72160024', NULL, 'KRISTIAN ANGGA PRIHATMOKO', 'Laki-laki', 'CILACAP', '1996-05-03', 'A', 'Kristen', '081329453550', '081329117373', 'DUSUN GUNUNGSARI RT 02/04, KAWUNGANTEN LOR, KAWUNGANTEN, CILACAP', '-', 'SMA YOS SUDARSO CILACAP', 'CILACAP', 2, 'L', 'Tidak Ada Penyakit', 'DINGIN', 'BRONKITIS, TIPES', 'MAKANAN LAUT', 'PUSING ', 'SESAK', 'TEOSAL', 'Tidak', '2016-06-29 09:46:50', '2016-06-29 09:46:50'),
(264, '71160003', NULL, 'ALDO KURNIA WICAKSONO', 'Laki-laki', 'YOGYAKARTA', '1998-08-24', 'B', 'Kristen', '089682310184', '0811293465', 'JALAN JAMBON PERUM MULYO ASRI NO 7', 'JALAN MONJALI 93A', 'SMA BOPKRI 1 YOGYAKARTA', 'YOGYAKARTA', 1, 'S', 'Tidak Ada Penyakit', 'DEBU', '-', 'UDANG', 'GATAL', '-', '-', 'Tidak', '2016-06-29 09:49:27', '2016-06-29 09:49:27'),
(265, '72160025', NULL, 'BRIAN ELDRIN SOMBUK', 'Laki-laki', 'BIAK', '1998-03-19', '', 'Kristen', '081226314259', '085254104339', 'KAMPUNG ANGGOPI JL BARITO RAYA RT 003 DESA ANGGOPI KECAMATAN ORIDEK, BIAK, PAPUA', 'JLN MELON MUDUDAREN NO.173 PONDOK BAITI KLEDOKAN', 'SMK YPK 1 BIAK', 'BIAK', 2, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', 'IKAN ', 'GATAL-GATAL', 'NYERI', 'OBAT MAAG DARI RUMAH SAKIT', 'Ya', '2016-06-29 10:08:40', '2016-06-29 10:08:40'),
(266, '12160037', NULL, 'FANUEL PAULY RONI AROMGGEAR LEPAR', 'Laki-laki', 'MANOKWARI', '1997-07-22', 'O', 'Kristen', '085254073398', '082197927472', 'MANOKWARI,PAPUA BARAT JALAN SORONG KAMPUNG MOWBJA DISTRIK MANSI', 'JLN. SETURAN PERUMAHAN APH\r\nBLOK E 18', 'SMA N 1 PRAFI', 'MANOKWARI', 7, 'S', 'Tidak Ada Penyakit', '-', 'ANEMIA', 'IKAN ASIN. TERONG', 'GATAL\" DAN BENJOL\"', 'PUSING\"', 'SANGOBION', 'Ya', '2016-06-29 10:14:32', '2016-06-29 10:14:32'),
(267, '41160033', NULL, 'BRAMANTYA SAMUEL RIZKI ARGIYANTO', 'Laki-laki', 'WONOGIRI', '1995-09-05', 'B', 'Kristen', '08562820613', '081329503174', 'TEGALWINANGUN RT03/RW13 TEGALGEDE, KARANGANYAR, SURAKARTA, JAWA TENGAH', 'PERUMAHAN PILAHAN ASRI NO. 40, RT 45/RW 11, REJOWINANGUN, KOTAGEDE, YOGYAKARTA', 'SMA 1 SURAKARTA', 'SURAKARTA', 10, 'L', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-29 10:46:43', '2016-06-29 10:46:43'),
(268, '41160048', NULL, 'ADITYA ARISTO MARVEL NUGROHO', 'Laki-laki', 'SURABAYA', '1998-06-11', 'O', 'Katholik', '085228158444', '08122700740', 'JL. KALIURANG KM 5,7 GG PANDEGASIWI NO 9 RT 017/ RW 01', 'JL. KALIURANG KM 5,7 GG PANDEGASIWI NO 9 RT 017/ RW 01', 'SMA BOPKRI 1 YOGYAKARTA', 'YOGYAKARTA', 10, 'XL', 'Tidak Ada Penyakit', '-', 'DB, USUS BUNTU', '-', '-', 'VERTIGO, NYERI', '-', 'Ya', '2016-06-29 11:19:57', '2016-06-29 11:19:57'),
(269, '71160017', NULL, 'ABEDNEGO PETRA PRASETYA', 'Laki-laki', 'KLATEN', '1999-03-11', 'O', 'Kristen', '085641346465', '085325999611', 'JOGOYUDAN JT 3/689 RT 045/RW 011 GOWONGAN, JETIS, YOGYAKARTA', 'JOGOYUDAN JT 3/689 RT 045/RW 011 GOWONGAN, JETIS, YOGYAKARTA', 'SMA N 2 MAGELANG', 'MAGELANG', 1, 'XXXL', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Tidak', '2016-06-29 11:22:24', '2016-06-29 11:22:24'),
(270, '61160053', NULL, 'JIMMY MACHAEL TIRTAYASA', 'Perempuan', 'BLORA', '1998-09-23', 'O', 'Katholik', '082248019932', '081330704488', 'JL RAYA MANDALA, MERAUKE', 'TAMBAKBAYAN 7/10A', 'SMA 1 MERAUKE', 'MERAUKE', 6, 'M', 'Tidak Ada Penyakit', '-', 'MALARIA', '-', '-', 'DEMAM DAN MUAL', '-', 'Ya', '2016-06-29 11:24:45', '2016-06-29 11:24:45'),
(271, '71160054', NULL, 'IVANA N DJOJAGA', 'Perempuan', 'TOBELO', '1998-12-24', 'O', 'Kristen', '081342291010', '082187610145', 'TOBELO - HALMAHERA UTARA (DESA GURA)', 'JL MELATI WETAN, KELURAHAN BACIRO, KEC. GONDOKUSUMAN', 'SMA KRISTEN TOBELO', 'TOBELO', 1, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-29 11:26:44', '2016-06-29 11:26:44'),
(272, '71160055', NULL, 'RATRI PRIHATI NINGTYAS', 'Perempuan', 'BELITANG', '1997-11-05', 'O', 'Kristen', '085783837585', '085658924116', 'DS. HARJOWINANGUN RT2/RW2 BELITANG, OKU TIMUR, SUMSEL', 'LEMPUYANGAN', 'SMK PGRI 2 BELITANG', 'BELITANG', 1, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', 'TELUR', 'BENTOL DI KELOPAK MATA BAWAH', 'NYERI', 'GRASTROSIT', 'Ya', '2016-06-29 11:29:00', '2016-06-29 11:29:00'),
(273, '41160049', NULL, 'STEFANUS WIGUNA TERIAWAN', 'Laki-laki', 'SEMARANG', '1998-11-08', 'A', 'Kristen', '082243595280', '08122602839', 'JL TLEGOSARI SELATAN , BLOK H NO 15 SEMARANG', '-', 'SMA KARANGTURI', 'SEMARANG', 10, 'L', 'Tidak Ada Penyakit', 'DEBU', 'ASMA', '-', 'BERSIN-BERSIN', 'SESAK NAPAS', 'INHEALER', 'Ya', '2016-06-29 11:31:50', '2016-06-29 11:31:50'),
(274, '61160051', NULL, 'ADITYA MAHENDRA', 'Laki-laki', 'SLEMAN', '1998-08-17', 'A', 'Islam', '088806075220', '0816686865', 'PERUMAHAN BANTENG BARU ADHYAKSA 3 NO 62 JL KALIURANG KM 7,8', 'PERUMAHAN BANTENG BARU ADHYAKSA 3 NO 62 JL KALIURANG KM 7,8', 'SMA TIGA MARET', 'YOGYAKARTA', 6, 'S', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-29 11:49:25', '2016-06-29 11:49:25'),
(275, '71160056', NULL, 'ALFRIAN PURBA', 'Laki-laki', 'MERAUKE', '1998-12-11', 'B', 'Kristen', '082199263438', '081247038984', 'JALAN MAPI, KEL. MARGO RT 14/RW 04, DESA MARO, MERAUKE', 'GG. KOLOBENDUWO NO.14 DRINGGODANI RT. 15/RW 06 KODYA YOGYAKARTA', 'SMA NEGERI 1 MERAUKE', 'MERAUKE', 1, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-29 11:59:52', '2016-06-29 11:59:52'),
(276, '72160026', NULL, 'JULIAN EDUARD BISAI', 'Laki-laki', 'SERUI', '1997-09-26', 'A', 'Kristen', '082138007172', '081382080208', 'JLN. HASANNUDIN SERUI RT/RW 003/004 SERUI KOTA, YAPEN SELATAN, PAPUA', 'SELOKAN MATARAM', 'SMA NEGERI 1 SERUI', 'SERUI', 2, 'S', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'NYERI', 'PROMAG', 'Tidak', '2016-06-29 12:36:14', '2016-06-29 12:36:14'),
(277, '41160051', NULL, 'KENJI SEBASTIAN HALIM', 'Laki-laki', 'SUKOHARJO', '1999-01-21', 'AB', 'Katholik', '083865995006', '0811285550', 'JL. DR. SUPOMO NO.3 RT. 04/ RW. 05\r\nSURAKARTA', '-', 'TAYLOR\'S COLLEGE', 'MALAYSIA', 10, 'M', 'Tidak Ada Penyakit', '-', '-', '-', '-', '-', '-', 'Ya', '2016-06-29 12:43:56', '2016-06-29 12:43:56'),
(278, '12160038', NULL, 'MEILANESIA PAPUANI RAMANDEY', 'Perempuan', 'JAYAPURA', '1999-05-17', 'O', 'Kristen', '082136806297', '082298744488', 'UREI FAISEI I WAROPEN PAPUA', 'PERUM GRIYA TIMOHO ESTATE NO. 27 KELURAHAN MUJAMUJU KECAMATAN GONDOKUSUMAN YOGYAKARTA', 'SMA NEGERI 3 JAYAPURA', 'JAYAPURA', 7, 'XL', 'Tidak Ada Penyakit', '-', 'MAAG', '-', '-', 'NYERI', 'MYLANTA', 'Ya', '2016-06-29 13:00:55', '2016-06-29 13:00:55'),
(279, '61160054', NULL, 'ALAN CHOLIN PARENTA', 'Laki-laki', 'KOLONODALE', '1999-07-08', 'A', 'Kristen', '082349991488', '082342453821', 'RONTA, KEC. LEMBO RAYA, KAB. MOROWALI UTARA, SULAWESI TENGAH.', '-', 'SMA KRISTEN 2 (BINSUS) TOMOHON', 'TOMOHON', 6, 'S', 'Tidak Ada Penyakit', '-', 'TIPES', '-', '-', 'BADAN PANAS', 'KAPSUL CACING', 'Ya', '2016-06-29 13:08:36', '2016-06-29 13:08:36'),
(280, '72120007', NULL, 'Uchiha', 'Laki-laki', 'Makassar', '2007-05-29', 'A', 'Budha', '086766234624', '089823684623', 'Jogja', 'Jogja', 'SMA 1', 'Yogyakarta', 7, 'M', 'Penyakit Ringan', 'Tidak', 'Penyakit', 'Ada', 'Reaksi', 'Reaksi', 'Pereda', 'Ya', '2017-05-29 14:51:25', '2017-05-29 14:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `pma_bookmark`
--

CREATE TABLE `pma_bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma_column_info`
--

CREATE TABLE `pma_column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma_designer_coords`
--

CREATE TABLE `pma_designer_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `v` tinyint(4) DEFAULT NULL,
  `h` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma_history`
--

CREATE TABLE `pma_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma_navigationhiding`
--

CREATE TABLE `pma_navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma_pdf_pages`
--

CREATE TABLE `pma_pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma_recent`
--

CREATE TABLE `pma_recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma_recent`
--

INSERT INTO `pma_recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"okadb\",\"table\":\"pesertas\"},{\"db\":\"okadb\",\"table\":\"kelompoks\"},{\"db\":\"phpmyadmin\",\"table\":\"pma_column_info\"},{\"db\":\"phpmyadmin\",\"table\":\"pma_designer_coords\"},{\"db\":\"phpmyadmin\",\"table\":\"pma_history\"},{\"db\":\"phpmyadmin\",\"table\":\"pma_pdf_pages\"},{\"db\":\"phpmyadmin\",\"table\":\"pma_recent\"},{\"db\":\"phpmyadmin\",\"table\":\"pma_relation\"},{\"db\":\"cdcol\",\"table\":\"cds\"},{\"db\":\"mysql\",\"table\":\"user\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma_relation`
--

CREATE TABLE `pma_relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma_savedsearches`
--

CREATE TABLE `pma_savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma_table_coords`
--

CREATE TABLE `pma_table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma_table_info`
--

CREATE TABLE `pma_table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma_table_uiprefs`
--

CREATE TABLE `pma_table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma_tracking`
--

CREATE TABLE `pma_tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `pma_userconfig`
--

CREATE TABLE `pma_userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma_userconfig`
--

INSERT INTO `pma_userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2015-06-05 03:13:36', '{\"collation_connection\":\"utf8mb4_general_ci\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma_usergroups`
--

CREATE TABLE `pma_usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma_users`
--

CREATE TABLE `pma_users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

-- --------------------------------------------------------

--
-- Table structure for table `prodis`
--

CREATE TABLE `prodis` (
  `id` int(2) NOT NULL,
  `nama_prodi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodis`
--

INSERT INTO `prodis` (`id`, `nama_prodi`) VALUES
(1, 'Teknik Informatika'),
(2, 'Sistem Informasi'),
(3, 'Bioteknologi'),
(4, 'Teologi'),
(5, 'Desain Produk'),
(6, 'Arsitektur'),
(7, 'Akuntansi'),
(8, 'Manajemen'),
(10, 'Kedokteran'),
(12, 'Bahasa Inggris');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(512) NOT NULL,
  `role` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `created`, `modified`) VALUES
(1, 'adminoka', '$2a$10$qaLSvKvgSnts8a.60ybq8OOPpWBz1NomWj07bIkQWEV7n3tVQEu0.', 'admin', '2015-06-10 22:27:51', '2015-06-10 22:27:51'),
(2, 'useroka1', '$2a$10$LNY6nvRj.T.oSG6jBDzcEOWVzdqMaFWcNkzpCeaXHFuPITKbn2txW', 'user', '2015-06-10 22:29:05', '2015-06-11 12:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_pwd`
--

CREATE TABLE `user_pwd` (
  `name` char(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user_pwd`
--

INSERT INTO `user_pwd` (`name`, `pass`) VALUES
('xampp', 'wampp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cds`
--
ALTER TABLE `cds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelompoks`
--
ALTER TABLE `kelompoks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesertas`
--
ALTER TABLE `pesertas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma_bookmark`
--
ALTER TABLE `pma_bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma_column_info`
--
ALTER TABLE `pma_column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma_designer_coords`
--
ALTER TABLE `pma_designer_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma_history`
--
ALTER TABLE `pma_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma_navigationhiding`
--
ALTER TABLE `pma_navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma_pdf_pages`
--
ALTER TABLE `pma_pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma_recent`
--
ALTER TABLE `pma_recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma_relation`
--
ALTER TABLE `pma_relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma_savedsearches`
--
ALTER TABLE `pma_savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma_table_coords`
--
ALTER TABLE `pma_table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma_table_info`
--
ALTER TABLE `pma_table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma_table_uiprefs`
--
ALTER TABLE `pma_table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma_tracking`
--
ALTER TABLE `pma_tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma_userconfig`
--
ALTER TABLE `pma_userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma_usergroups`
--
ALTER TABLE `pma_usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma_users`
--
ALTER TABLE `pma_users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- Indexes for table `prodis`
--
ALTER TABLE `prodis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_pwd`
--
ALTER TABLE `user_pwd`
  ADD PRIMARY KEY (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cds`
--
ALTER TABLE `cds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kelompoks`
--
ALTER TABLE `kelompoks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `pesertas`
--
ALTER TABLE `pesertas`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;
--
-- AUTO_INCREMENT for table `pma_bookmark`
--
ALTER TABLE `pma_bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma_column_info`
--
ALTER TABLE `pma_column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma_history`
--
ALTER TABLE `pma_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma_pdf_pages`
--
ALTER TABLE `pma_pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma_savedsearches`
--
ALTER TABLE `pma_savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prodis`
--
ALTER TABLE `prodis`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
